# coding: utf-8

import json
import sys

from benritools import ShColor

from flask import Flask, jsonify, make_response, request

from icecream import ic


# 駄洒落系のロード
from lcv import DajaRecognizer

api = Flask(__name__)
t0 = 0.0
HOST = "10.0.2.15"
PORT = 8088


djrc = DajaRecognizer("trained.model", "traindata40k.txt")


@api.errorhandler(400)
@api.errorhandler(500)
def onError(error):
    ic(error.code)
    ic(error.description)


# 入力発話をPOSTで受け取って判定結果JSONを返す　
@api.route('/dajarecog', methods=['POST'])
def post():
    # POST で送られてきた情報を取得．
    data_raw = request.get_data(as_text=True)
    ic("--" * 10)
    ic(data_raw, type(data_raw))#d
    data = json.loads(data_raw, encoding='utf-8')
    uttr = data["uttr"]
    ic(uttr)#d
    result_value = djrc.checkDajare(uttr)
    led = ShColor.YELLOW + "    !!! DAJARE DETECTED !!!" + ShColor.END\
        if result_value == '2' else ""
    print(f"{ShColor.CYAN}** RESULT: {result_value}{ShColor.END}{led}",
          file=sys.stderr)
    result_dict = {"result": result_value}
    return make_response(jsonify(result_dict))


if __name__ == '__main__':
    api.run(host=HOST, port=PORT)
