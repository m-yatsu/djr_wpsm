#!/usr/bin/env python
# coding: utf-8
"""Japanese tokenizer module for classification"""

from __future__ import print_function
import codecs
import sys

import MeCab

import gensim
from gensim.corpora.dictionary import Dictionary
#from benritools import report_progress

ENC = "utf-8"
DIC_TYPE = "ipadic"
POS_ID = [0, 1, 2] + list(range(10, 69))
tagger = MeCab.Tagger()


def token_cond(node):
    return node.posid in POS_ID


def token_ftr_lemma(ftr):
    if DIC_TYPE == "ipadic":
        idx = 6
    else:
        idx = 6
    return ftr[idx]


def get_node_features(node):
    #if DIC_TYPE == "ipadic":
    #    return [f for f in node.feature.decode(ENC).split(u',')]
    return [f for f in node.feature.split(u',')]


def tokenize(text):
    """
    Yields:
        lemma of a word in input sentence text (unicode)
    """
    #node = tagger.parseToNode(text.encode(ENC))
    node = tagger.parseToNode(text)
    while node.next:
        node_lemma = token_ftr_lemma(get_node_features(node))
        node = node.next
        if node_lemma != u"*" and token_cond(node):
            #print("OK")
            yield node_lemma


def tokenize_spaced(text):
    return text.split(u" ")


def bag_of_words(text, vocab, use_mecab=True):
    """
    Args:
        text:str
        vocab:gensim.corpora.Dictionary
    Returns:
        a numpy array
    """
    # Tokenize text
    text_tokenized = tokenize(text) if use_mecab else tokenize_spaced(text)
    # Extract bag of words
    corpus = vocab.doc2bow(text_tokenized, allow_update=False, return_missing=False)
    # Convert bow to dense vector
    #print("C:", corpus, file=sys.stderr)#d
    dense = gensim.matutils.corpus2dense([corpus], num_terms=len(vocab))
    return dense.T[0]


def one_hot_sequence(text, vocab, use_mecab=True):
    for token in tokenize(text):
        vec_hot = bag_of_words(token, vocab, use_mecab)
        #print(vec_hot, sum(vec_hot)) #d
        #raw_input(":")
        yield vec_hot


def one_hot_sequence_iter(fname_data, vocab, use_mecab=True):
    for l_ in codecs.open(fname_data, encoding=ENC):
        for vec_hot in one_hot_sequence(l_.strip(), vocab, use_mecab):
            yield vec_hot


def tokenizeTEST(text):
    return text.strip().split()


def create_vocabularyTEST(fname, v_size=20000, report=False):
    vocab = Dictionary()
    try:
        with codecs.open(fname, encoding=ENC) as fpr_datalrn_0:
            for i, l_ in enumerate(fpr_datalrn_0):
                #report_progress(i)
                line = l_.strip()
                vocab.add_documents([[token for token in tokenizeTEST(line)]])
    except Exception:
        print("ERROR while preparing BoW dictionary", file=sys.stderr)
        raise
    #vocab.filter_extremes(keep_n=v_size)
    #vocab.compactify()
    if report:
        return vocab, i + 1
    else:
        return vocab


def main():
    stc = u"今日こそ打ち上げたいので頑張ってるとのこと"
    print(stc)
    print(",".join(tokenize(stc)))
    return 0


if __name__ == "__main__":
    sys.exit(main())
