# coding: utf-8
"""前処理"""


class Tr:
    """文字レベルの前処理に用いる関数"""
    re_katakana = re.compile(r'[ァ-ヴ]')
    re_hiragana = re.compile(r'[ぁ-ゔ]')
    re_non_hiragana = re.compile(r' ?[^ぁ-ゔ ] ?')
    re_kakko = re.compile(r'[(（][^()（）]+[)）『』「」]')
    re_pre2 = re.compile(r'[．。，、！？・＋＿；：＊・”’＝/／☆！？!?]')

    tbl_zenhan_number = {ord(n_wide): str(n_narrow)
                         for n_wide, n_narrow
                         in zip('０１２３４５６７８９', range(0, 10))}

    @classmethod
    def hiragana(cls, text: str) -> str:
        """ひらがなに変換"""
        return cls.re_katakana.sub(lambda x: chr(ord(x.group(0)) - 0x60), text)

    @classmethod
    def katakana(cls, text: str) -> str:
        """カタカナに変換"""
        return cls.re_hiragana.sub(lambda x: chr(ord(x.group(0)) + 0x60), text)

    @classmethod
    def hira_only(cls, text: str) -> str:
        """ひらがなに変換し，ひらがな以外を除去"""
        text = cls.hiragana(text)
        text = cls.re_non_hiragana.sub(u' ', text).strip()
        return text

    @classmethod
    def kakko(cls, text: str) -> str:
        """括弧を除去"""
        text = cls.re_kakko.sub(u'', text).strip()
        return text

    @classmethod
    def pre2(cls, text: str) -> str:
        """約物（句読点等）を除去"""
        text = cls.re_pre2.sub(u'', text).strip()
        text = text.translate(cls.tbl_zenhan_number)
        return text


# Static class
class Preprocess:
    """入力文の前処理を行うクラス．
    入力文が駄洒落コーパスのアノテーションを含む場合，これを維持する．
    """

    @staticmethod
    def process_dcdata():
        """パース済みのDC事例を受け取り，dataに前処理結果を格納する．"""
        pass

    @staticmethod
    def by_morpheme():
        """形態素分割された，
        漢字かな交じり文字列の2Dリストの要素リストを形態素として前処理する．
        各形態素に対して読みへの変換を行い，文字レベルのクリーンアップ処理を行う．
        その後にモーラ列へ変換し，各モーラ列に対し音便処理を行う"""
        pass

    @staticmethod
    def by_char():
        pass

    @staticmethod
    def by_mora():
        pass
