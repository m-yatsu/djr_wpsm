
子音・母音音韻類似度PMIの算出法
====

## 1. <PSR に用いる子音&母音類似度行列の作成>

### 1a.  種形態素のモーラ音素列表現(mpe)の取得

 * アノテーション記載による単独または連続形態素
 * 変形表現のmpeを区間から取得/変換

### 1b.  種・変形各mpeの，完全一致による母音部アライメント

 * アライメントアルゴリズム(NWA)を使用（グローバルアライメント）
   - Needleman-Wunsch
   - Smith-Waterman
   - FOGSAA (Fast Optimal Global Sequence Alignment Algorithm)

### 1c.  子音PMIの算出

 * 分子：アライメントにより得られた子音ペア対応を共起頻度
 * 分母：ペア各子音の単独生起頻度の積
 * (分母類似度算出のためのWPSMに利用する --> WPSM\_C)

### 1d. WPSM\_Cと下記基準に基づいて子音部をアライメント

 * 閾値以上のもの及び完全一致
 * アルゴリズム及びパラメタは1bと同一のものを利用．
   -  (子音アライメントとの整合性を保つため)

### 1e. 母音PMIの算出

 * (手法は1c) --> WPSM\_v

## 2. <各素性の実装>

### 2a.  子音・母音PMIを用いた検出(PSR)によるルールベースonly検出手法

#### 検出手法

 1. 入力文MA結果中のそれぞれの種表現候補(形態素)について，
入力文とのローカルアライメント<子音・母音>を行う．
   - アルゴリズムはSmith-Waterson.(SWA)
   - この時のWPSM\_c, WPSM\_vが子音と母音それぞれの A. のsubstitution matrix になる．
   - 子音Aの結果(アライメント成立箇所)をC, 母音Aの結果をVとしたとき，
計算に用いる部分はCとVの組み合わせにより定まる．
     + CとVの組み合わせを次の通り用意し，開発データに対する実験から最適手法を選択:

    A: (CとVの共通部分)
    B: A ＋ (C のみの部分)

検出の手順は，以下：

1. 上記A or Bの部分のアライメントにおける各音素対の子音類似度・母音類似度を
  WPSM\_C と WPSM\_V から夫々求めて総和
1. 配列長で正規化
1. 閾値と比較．

### 2b. BOW

### 2c. Ngram-LM

### 2d. RNN-LM

## 3. <分類器の構成・素性の組み合わせ and 実験条件>

### 3a.  子音・母音PMIを用いた検出(PSR)を，ルールベースonly検出手法とする

 ルールベース検出に，BOWを加えたものが手法2．（PSR+BOW, ファジィ提案手法）
 さらに，変形部分のモーラN-gram-LMでの尤度(HLML)も加える．
 さらに，変形部分のモーラRNN-LMでの尤度(HNNL)も加える．

 上記素性を一覧すれば，PSR, BOW, HLML, HNNL．
 これらによる実験対象の素性組み合わせで，比較用として有用なのは，

1. FS\_a PSR
1. FS\_b PSR+BOW
1. FS\_c PSR+BOW+HLML
1. FS\_d PSR+BOW+HNNL
1. FS\_e PSR+BOW+HNNL+HNNL
1. FS\_f BOW
1. FS\_g BOW+HLML
1. FS\_h BOW+HNNL
1. FS\_i BOW+HLML+HNNL
1. FS\_j PSR+HLML
1. FS\_k PSR+HNNL
1. FS\_l PSR+HLML+HNNL
1. FS\_m HLML+HNNL

の13種となる．

