#!/usr/bin/env python
# coding: utf-8
"""DJRE LCV: Learn, Classify and (cross-)Validate

Learn: from a copy of Dajare Corpus.
Classify: using the trained model.
Validate: using the trained model and (cross-)validation data.

Usage:
    python3 lcv.py <mode> <arguments> <options>

    See main() for detailed arguments / options.
"""

import codecs
import pickle
import sys
import time
from argparse import ArgumentParser

from benritools import MyTimer, printe, report_progress, timer_start, timer_stop

from dc_parser import DCParser  # Dajare Corpus paresr

import features as S  # Feature functions

from gensim.corpora import Dictionary

import numpy as np

from sklearn.model_selection import StratifiedKFold, cross_val_score
from sklearn.svm import LinearSVC as SL_0
from sklearn.svm import SVC as SL_1


ENC = "utf-8"
FPATH_INI = "config.ini"
MODEL_DEFAULT = "trained.model"
VOCAB_DEFAULT = "dajare20k.txt"

VLD_K = 10
FEATURES = [S.ftrG_bow, S.ftr_phonsim_c, S.ftrG_phonsim_lmrnn]
FEATURESETS_DEFAULT = [
    ('BOW',             [0]),
    ('BOW & PhonSim',   [0, 1]),
    ('BOW & PhonRNNLM', [0, 2]),
    ('BOW, PhonSim & RNNLM', [0, 1, 2]),
]


def fs(lst):
    return [FEATURES[i] for i in lst]


class MyClassifier(object):
    """For purposes including training, evaluating (single or multiple
    examples) and cross-validation."""
    def __init__(self, kernel='linear', vocab=Dictionary()):
        self.feature_funcs = [
            S.ftrG_bow,
            S.ftr_phonsim_c,
            #S.ftrG_phonsim_lmrnn,
        ] # arg: text, returns: feature value (float64)
        self.vocab = Dictionary()
        self.saved = []
        self.init_clf(kernel)
        self.kernel = kernel

    def set_features(self, lst_func):
        self.feature_funcs += lst_func

    def init_clf(self, kernel):
        """Initializes internal classifier."""
        if kernel == 'linear':
            self.clf = SL_0()
        else:
            self.clf = SL_1(kernel=kernel)

    def set_feature_funcs(self, lst_funcs):
        self.feature_funcs = lst_funcs

    def get_bow_dict(self):
        return self.vocab

    def create_vocab_fromCorpus(self, fpath_corpus):
        """Extracts vocabulary from a corpus compatible to DC.
        Arg:
            fpath_corpus: The file path of DC corpus.
        Returns:
            int: Number of lines processed. (for time measurement)"""
        assert fpath_corpus != "", "Filename is not provided!"
        n_lines = 0
        try:
            dcp = DCParser(fpath_corpus)
            for n, (label, line_tokens) in enumerate(dcp.parse_raw_vocab(), 1):
                self.vocab.add_documents([line_tokens])
                n_lines = n
        except Exception as ex:
            printe("ERROR while preparing BoW dictionary:", str(ex))
            raise
        return n_lines # for time measurement

    def calc_values_fromCorpus(self, fpath_corpus):
        X = None
        y = []
        time_prev = [time.time(), time.time()]  # using a list because it is mutable
        try:
            with codecs.open(fpath_corpus, encoding=ENC) as fpr_data:
                for i, l_ in enumerate(fpr_data):
                    report_progress(i)
                    report_progress(i, 1000, True, time_prev, only_time=True)
                    vector = np.array((1, 0))
                    lss = l_.strip().split()
                    label, stc = int(lss[0]), lss[1]
                    # Actual feature values depend on feature list
                    for func in self.feature_funcs:
                        feature_values = func(stc, self.vocab)
                        vector = np.hstack([vector, feature_values])
                    vector_np = np.array(vector).reshape((1, len(vector)))
                    if X is None:
                        X = vector_np
                    else:
                        X = np.vstack([X, vector_np])
                    y.append(label)
        except Exception as ex:
            printe("=====\nERROR while loading data values:", str(ex))
            raise

        self.X = X
        self.y = np.array(y)
        return i + 1 # for time measurement

    def learn_from_file(self, fpath_datalrn="X_learn.txt"):
        """
        Args:
            fpath_datalrn: filepath of learning data

        Learning data:
            1入力文を1行ごとに改行したテキストファイル．
            正例は種及び変形表現のアノテーションをもつ行．
            負例は上記のアノテーションがなく分かち書きのみの行．
            正例負例ともに駄洒落コーパスのフォーマットに準拠する．
        """
        self.init_clf(self.kernel)
        x_train = []
        y_train = []
        # Prepare BoW dictionary
        if not self.vocab or len(self.vocab) < 10:
            with MyTimer("Creating vocabulary (for BoW)") as t:
                t.itrs = self.create_vocab_fromCorpus(fpath_datalrn)

        time_prev = [time.time(), time.time()] # using a list because it is mutable
        printe("\nCalculating feature values of train data")
        # Calculate feature value for each feature function
        try:
            dcp = DCParser(fpath_datalrn)
            for i, label, wakati in enumerate(dcp.parse_raw_vocab()):
                report_progress(i)
                report_progress(i, 1000, True, time_prev, only_time=True)
                vector = []
                stc = "".join(wakati)
                for func in self.feature_funcs:
                    feature_values = func(stc, self.vocab)
                    vector = np.hstack([vector, feature_values])

                x_train.append(vector)
                y_train.append(label)
            with open(fpath_datalrn + ".ftrvals.pickle", "wb") as fpw:
                pickle.dump((x_train, y_train), fpw) # MemoryError here
        except Exception as ex:
            printe("=====\nERROR while loading data values:", str(ex))
            raise
        try:
            with open(fpath_datalrn + ".ftrvals.pickle", "wb") as fpw:
                pickle.dump((x_train, y_train), fpw)
        except Exception as ex:
            printe("=====\nERROR while saving data values:", str(ex))
            raise
        # Train the model
        with MyTimer("Training"):
            self.clf.fit(x_train, y_train)
        printe("Trained the model using: '" + fpath_datalrn + "'")
        return

    def save_model(self, fpath_model):
        try:
            with open(fpath_model, "wb") as fpw:
                pickle.dump(self.clf, fpw)
        except Exception as ex:
            printe("=====\nERROR while saving model:", str(ex))
            raise
        printe("Saved the model to: '" + fpath_model + "'")

    def load_model(self, fpath_model):
        try:
            with open(fpath_model, "rb") as fpr:
                self.clf = pickle.load(fpr)
        except Exception as ex:
            printe("=====\nERROR while loading model:", str(ex))
            raise
        printe("Loaded the model from: '" + fpath_model + "'")

    def save_data(self, fpath_obj, report=True):
        tpl_saved_obj = (self.X, self.y)
        try:
            with open(fpath_obj, 'wb') as fpw:
                pickle.dump(tpl_saved_obj, fpw)
        except Exception as ex:
            printe("=====\nERROR while saving data:", str(ex))
            raise
        self.saved.append(fpath_obj)
        if report:
            print("Dumped data file: " + fpath_obj)

    def load_data(self, fpath_obj, report=True):
        """Loads and deserializes an object from a file."""
        try:
            with open(fpath_obj, 'rb') as fpr:
                tpl_loaded_obj = pickle.load(fpr)
        except Exception as ex:
            printe("=====\nERROR while leading data:", str(ex))
            raise
        self.X, self.y = tpl_loaded_obj
        if report:
            print("Loaded data file: " + fpath_obj)
        self.saved.append(fpath_obj)

    def classify_from_file(self, fpath_datatst):
        """
        Args:
            fpath_datalrn: filepath of test data
        """
        assert self.saved != [], "Model has not been trained yet!"

        printe("\nCalculating feature values of test data")
        # Calculate feature value for each feature function
        x_test, y_test = [], []
        time_prev = time.time()
        try:
            with codecs.open(fpath_datatst, encoding=ENC) as fpr_datatst:
                for i, l_ in enumerate(fpr_datatst):
                    report_progress(i)
                    time_prev = report_progress(i, 1000, show_time=True, time_prev=time_prev)
                    vector = []
                    lss = l_.strip().split()
                    label, stc = int(lss[0]), lss[1]
                    for func in self.feature_funcs:
                        vector += func(stc, self.vocab)

                    x_test.append(vector)
                    y_test.append(label)
        except Exception as ex:
            printe("=====\nERROR while calculating feature values (test):", str(ex))
            raise

        # Do classification
        with MyTimer("Classification"):
            score = self.clf.score(x_test, y_test)
        printe("SCORE:", score)

    def classify_single(self, text):
        ipt_vec = []
        for func in self.feature_funcs:
            ipt_vec += func(text, self.vocab)
        ipt_vec_arr = np.array(ipt_vec).reshape(1, -1)
        result = self.clf.predict(ipt_vec_arr)
        return result[0]

    def classify(self, text):
        """
        Args:
            text as str: Input text.
        Returns:
            result as bool: True if a pun is detected in 'text'.
        """
        result = self.clf.classify()
        result = True
        return result

    def cross_validate(self, fpath_datavld='', fs_idx=[0], K=10):
        """Performs K-fold cross validation.

        Args:
            fpath_datavld: filepath of train/test data
            K: number of folds
        Returns:
            double: average of scores (accuracy rate) of K tests.
        """
        # Prepare BoW dictionary: self.vocab
        if not self.vocab:
            with MyTimer("N: Creating vocabulary (for CV)", itername="stc") as t:
                t.itrs = self.create_vocab_fromCorpus(fpath_datavld)

        # Prepare feature values and class labels: self.X, self.y
        fpath_datavld_obj = "{0}.f{1}.pickle".format(fpath_datavld,
                                                     "".join([str(idx) for idx in fs_idx]))
        if fpath_datavld_obj not in self.saved:
            with MyTimer("N: Creating data (for CV)") as t:
                t.itrs = self.calc_values_fromCorpus(fpath_datavld)
        else:
            self.load_data(fpath_datavld_obj)

        # Use NumPy array to apply sklearn.cross_validation.KFold
        #X_vld_array, y_vld_array = np.array(self.X), np.array(self.y)
        X_vld_array, y_vld_array = self.X, self.y
        k_fold = StratifiedKFold(n=len(X_vld_array), n_folds=K, shuffle=True)
        with MyTimer("Cross-validation"):
            result_array = cross_val_score(self.clf, X_vld_array,
                                           y_vld_array, cv=k_fold)
            printe(f"{K}-fold cross validation result:\n", result_array)
        return sum(result_array) / float(len(result_array))


class DajaRecognizer(object):
    """Class providing pun detection function to be called externally."""
    def __init__(self, fpath_model=MODEL_DEFAULT, fpath_vocab=VOCAB_DEFAULT):
        self.mc = MyClassifier()
        self.mc.load_model(fpath_model)
        self.mc.create_vocab_fromCorpus(fpath_vocab)

    def checkDajare(self, uttrText):
        return self.mc.classify_single(uttrText)


def start_cv(fpath_datavld, valid_k=10):
    ftrsets_name_indices = FEATURESETS_DEFAULT
    kernels = ['linear', 'poly', 'rbf', 'sigmoid']

    voc = {}    # Vocabulary used for BOW features
    saved = []  # Saved data ??

    printe("Target data source:", fpath_datavld)
    for i, kernel in enumerate(kernels):
        for j, (fs_name, fs_idx) in enumerate(ftrsets_name_indices):
            printe("\n" + "=" * 79)
            printe("Kernel {0} -- Features {1}: {2}".format(kernel, j, fs_name))
            printe("-" * 79)

            fpath_datavld_obj = "{0}.f{1}.pickle".format(fpath_datavld, fs_idx)

            mc = MyClassifier(kernel=kernel, vocab=voc)
            mc.saved = saved
            mc.set_feature_funcs(fs(fs_idx))
            if i > 0:
                mc.load_data(fpath_datavld_obj)
            # Run cross-validation
            prec = mc.cross_validate(fpath_datavld, fs_idx, valid_k)
            if i == 0:
                mc.save_data(fpath_datavld_obj)
            if not voc:
                voc = mc.get_bow_dict()
            saved = mc.saved
            printe("Prec({0} FS{1}):{2}".format(kernel, fs_idx, prec))
            del(mc)


def start_train(fpath_learn, fpath_model):
    mc = MyClassifier()
    mc.learn_from_file(fpath_learn)
    mc.save_model(fpath_model)


def start_specify(fpath_learn, fpath_test):
    mc = MyClassifier()
    mc.learn_from_file(fpath_learn)
    mc.classify_from_file(fpath_test)
    mc.save_model(fpath_learn + ".tmp.model")


def start_classify(fpath_model="", input_stc=""):
    if input_stc == "":
        return 0
    dr = DajaRecognizer(fpath_model)
    return dr.checkDajare(input_stc)


def main():
    """Usage:
    python3 lcv.py --mode <mode> <arguments> <options>

    以下のものは設定ファイル config.ini (固定) から読み込む：
    ・WPSM 類似度行列のファイルパス（子音用および母音用）

    引数で指定：
      [指定必須] <mode> 実験のモード
          cv: K分割交差検定実験 [デフォルト指定]
          specify:  データ指定実験
          classify: 引数または標準入力に対し検出実行

    cv モード限定：
      -c, --datacross 交差検定実験時の学習＆テストデータ (指定必須)
      -k, --folds    交差検定実験時の分割数 [デフォルト=10]

    train モード限定：
      -l, --datatrain データ指定実験時の学習データ (指定必須)

    cv モード限定：
      -l, --datatrain データ指定実験時の学習データ (指定必須)
      -t, --datatest  データ指定実験時のテストデータ (指定必須)

    オプション：
     -m, --modelfile  学習済みモデルファイル指定 (デフォルト: './trained.model')
     -m, --vocabfile  語彙を読むテキスト指定 (デフォルト: './dajare20k.txt')
     -v, --verbose  読み込み文数と検出数，検出割合等の統計を出力
    """
    ap = ArgumentParser(prog="python3 lcv.py", description=main.__doc__)
    ap.add_argument("-m", "--modelfile", dest="fpath_model")
    ap.add_argument("-v", "--vocabfile", dest="fpath_vocab")
    ap.set_defaults(fpath_model="./trained.model")

    aps = ap.add_subparsers(dest="mode")
    aps_cv = aps.add_parser('cv', help="CV help")
    aps_train = aps.add_parser('train', help="CV help")
    aps_specify = aps.add_parser('specify', help="Specify help")
    aps_classify = aps.add_parser('classify', help="Classify help")

    aps_cv.add_argument("-k", "--folds", type=int, dest="folds")
    aps_cv.add_argument("-c", "--datacross", dest="data_cross", required=True)
    aps_cv.set_defaults(folds=VLD_K, mode="cv")

    aps_train.add_argument("-l", "--datatrain", dest="fpath_train", required=True)
    aps_train.set_defaults(mode="train")

    aps_specify.add_argument("-l", "--datatrain", dest="fpath_train", required=True)
    aps_specify.add_argument("-t", "--datatest", dest="fpath_test", required=True)
    aps_specify.set_defaults(mode="specify")

    aps_classify.add_argument("input_sentence")
    aps_classify.set_defaults(mode="classify")

    printe("") # 空行
    args = ap.parse_args(argv)
    ret_code = 0
    t_start = timer_start(True)
    if args.mode == 'cv' and args.data_cross != '':
        start_cv(args.data_cross, args.folds)
    elif args.mode == 'train':
        start_train(args.fpath_train, args.fpath_model)
    elif args.mode == 'classify':
        start_classify(args.fpath_model, args.input_stc)
    elif args.mode == 'specify':
        start_specify(args.fpath_model, args.fpath_train, args.fpath_test)
    else:
        printe("Invalid argument(s) or options are given!\n" + main.__doc__)
        ret_code = 1
    timer_stop(t_start)
    return ret_code


if __name__ == "__main__":
    argv = sys.argv[1:]
    sys.exit(main())
