Dajare-WPSM: 音韻類似性を考慮する教師あり機械学習を用いた駄洒落検出
====

**In development: 以下の説明における使用例には動作しないものが多く含まれています．**

# インストール方法

```
git clone https://gitlab.com/m-yatsu/djr_wpsm
```

## 必要な環境

* [MeCab](http://taku910.github.io/mecab/) バージョン0.98以上 (動作確認済み: 0.996)
* IPA辞書
* [Python](http://www.python.org) バージョン3.5以上 (動作確認済み: 3.6.5)
* 以下の Python モジュール (全て pip よりインストール可能):
"numpy, scikit-learn, mecab-python3, gensim, regex"

## 設定ファイル

はじめに ```config.ini.example``` をコピーまたは名称変更して ```config.ini``` を作成します．

```config.ini``` を編集して，

* MeCab の設定ファイル: ```mecabrc```
* 各データファイルの保管場所: ```base_dir```

を設定してください．

# 使用方法

## Python (3.x系) を用いて CUI から実行する場合

__単一コーパスファイルからのK分割交差検証 (K=10のとき)__
```
python lcv.py cv -k 10 -c <corpus data file>
```

__学習データ・テストデータを指定した分類性能評価__
```
python lcv.py specify -l <train data file> -t <test data file>
```

# 使用可能データの規格

下記のようなフォーマットに従う必要があります．

[駄洒落データベース](http://arakilab.media.eng.hokudai.ac.jp/~araki/dajare.htm)に準拠しています．

```
# comment
2162 (店頭)12 の [テント]1 、 [転倒]2 21
2163 セルジオ 、 ち 4
2171 (白菜) の ぬ か 漬 、 [はぁ 、 臭い] 2
2192 (血行) の よい [コケッコー] 1
```
