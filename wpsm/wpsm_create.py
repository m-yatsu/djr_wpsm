#!/usr/bin/env python
# coding: utf-8
"""Calculate WPSM matrix used for alignment of phonemes based on
phonetic similarities, using Dajare Corpus as the input.

This ia runnable as a standalone script.

Last major update: Wed Nov 28 15:53:38 JST 2018"""

from __future__ import print_function
import sys
import traceback
from collections import defaultdict
from math import log
from typing import Dict
from alignment.align_nw import PhonNeedlemanWunsch
from dc_parser import DCParser
import numpy as np
import pickle

np.set_printoptions(precision=1)
ENC_DC = "utf-8"


class WPSMBuilder(object):
    """A concrete factory class for Word-Phonetic."""

    def __init__(self, cofreq: Dict, freq_t: Dict, freq_h: Dict) -> None:
        """Constructor of WPSMBuilder.

        :param cofreq: Ordered coocurrence frequency of key tuple (e1, e2)
        :type cofreq: dict
        :param freq_t: Occurence frequency of tane expression (e1,)
        :type freq_t: dict
        :param freq_h: Occurence frequency of henkei expression (e1,)
        :type freq_h: dict
        """
        self.cofreq = cofreq
        self.freq_t = freq_t
        self.freq_h = freq_h
        self.lst_t = sorted(self.freq_t.keys())
        self.lst_h = sorted(self.freq_h.keys())
        self.vocab_t = dict()
        self.vocab_h = dict()

    def calc_wpsm_nPMI(self) -> np.ndarray:
        """Calculate and return a new WPSM matrix
        using normalized PMI method."""
        sum_cofreq = float(sum(self.cofreq.values()))
        wpsm_array = np.zeros((len(self.lst_t), len(self.lst_h)))
        for i, e1 in enumerate(self.lst_t):
            self.vocab_t[e1] = i
            for j, e2 in enumerate(self.lst_h):
                self.vocab_h[e2] = j
                if (e1, e2) not in self.cofreq:
                    wpsm_array[i, j] = 0.0
                    continue
                p_1to2 = self.cofreq[(e1, e2)] / sum_cofreq
                assert p_1to2 <= 1.0, f"Value error: p('{e1}', '{e2}') = {p_1to2:f} > 1.0"   
                pmi = log(sum_cofreq * self.cofreq[(e1, e2)] /
                        self.freq_t[e1] / self.freq_h[e2])
                npmi = pmi / (-1.0 * log(p_1to2))
                assert abs(npmi) <= 1.0, (f"ERR: abs(npmi('{e1}', '{e2}')) "
                    f"= {abs(npmi):.2f} > 1.0, log(pmi)={log(p_1to2):.2f}")

                wpsm_array[i, j] = npmi # nPMI
        return wpsm_array

    def calc_wpsm_nSoA(self) -> np.ndarray:
        """Calculate and return a new WPSM matrix using
        normalized SoA method."""
        sum_freq_h = sum(self.freq_h.values()) * 1.0
        sum_cofreq = sum(self.cofreq.values()) * 1.0
        wpsm_array = np.zeros((len(self.lst_t), len(self.lst_h)))
        for i, e1 in enumerate(self.lst_t):
            self.vocab_t[e1] = i
            for j, e2 in enumerate(self.lst_h):
                self.vocab_h[e2] = j
                if (e1, e2) not in self.cofreq:
                    wpsm_array[i, j] = 0.0
                    continue

                p_1to2_pos = self.cofreq[(e1, e2)] / sum_cofreq
                p_1to2_neg = (self.freq_t[e1] - self.cofreq[(e1, e2)]) / sum_cofreq
                if p_1to2_neg == 0:
                    wpsm_array[i, j] = 1.0
                    continue
                assert p_1to2_pos <= 1.0 and p_1to2_neg <= 1.0,\
                    (f"Value error: p('{e1}', '{e2}') = {p_1to2_pos:.2f} > 1.0 OR"
                    f" p('{e1}', not '{e2}') = {p_1to2_neg:.2f} > 1.0")
                pmi_pos = log(sum_cofreq * self.cofreq[(e1, e2)] /
                        self.freq_t[e1] / self.freq_h[e2])
                npmi_pos = pmi_pos / (-1.0 * log(p_1to2_pos))
                assert abs(npmi_pos) <= 1.0, (f"abs(npmi_pos) = {abs(npmi_pos):.2f} > 1.0, ["
                        f"pmi_pos = {log(p_1to2_pos):.2f}]")
                pmi_neg = log(sum_cofreq * (self.freq_t[e1] - self.cofreq[(e1, e2)]) /
                    self.freq_t[e1] / (sum_freq_h - self.freq_h[e2]))
                npmi_neg = pmi_neg / (-1.0 * log(p_1to2_neg))
                assert abs(npmi_neg) <= 1.0, (f"abs(npmi_neg) = {abs(npmi_neg):.2f} > 1.0, ["
                        f"pmi_neg = {log(p_1to2_neg):.2f}]")
                wpsm_array[i, j] = max((npmi_pos - npmi_neg) / 2.0, 0) # nSOA
        return wpsm_array

    def get_vocab(self):
        return self.vocab_t, self.vocab_h


def save_serialized(obj, basename: str, midname: str, msg: str = "") -> None:
    print("Saving " + msg)
    fname = basename + midname + ".pickle"
    try:
        with open(fname, "wb") as fpw0:
            pickle.dump(obj, fpw0)
    except Exception as ex:
        print("Error saving to {0}: {1}\n".format(fname, ex), file=sys.stderr)
        traceback.print_exc()


def load_wpsm(wpsm_basefname_cons, wpsm_basefname_vow):
    print("Loading WPSM")
    # WPSM/C matrix
    wpsmc = np.zeros(1)
    try:
        with open(wpsm_basefname_cons + ".matrix") as fpr:
            wpsmc = pickle.load(fpr)
    except Exception:
        traceback.print_exc()
    # WPSM/C vocabs (Tane-Henkei)
    wpsmc_vocabs_TH = (None, None)
    try:
        with open(wpsm_basefname_cons + ".vocab") as fpr:
            wpsmc_vocabs_TH = pickle.load(fpr)
    except Exception:
        traceback.print_exc()
    # WPSM/V matrix
    wpsmv = np.zeros(1)
    try:
        with open(wpsm_basefname_vow + ".matrix") as fpr:
            wpsmv = pickle.load(fpr)
    except Exception:
        traceback.print_exc()
    # WPSM/V vocabs (Tane-Henkei)
    wpsmv_vocabs_TH = (None, None)
    try:
        with open(wpsm_basefname_vow + ".vocab") as fpr:
            wpsmv_vocabs_TH = pickle.load(fpr)
    except Exception:
        traceback.print_exc()
    return wpsmc, wpsmv, wpsmc_vocabs_TH, wpsmv_vocabs_TH


def make_wpsm(fpath_dcorpus, wpsm_basefname_cons, wpsm_basefname_vow):

    # Aligner
    pa = PhonNeedlemanWunsch(match=2, mismatch=-1, gap=-1)

    # Data
    th_pairs = []
    freq_cons_t = defaultdict(int)
    freq_cons_h = defaultdict(int)
    cofreq_cons = defaultdict(int)
    freq_vow_t = defaultdict(int)
    freq_vow_h = defaultdict(int)
    cofreq_vow = defaultdict(int)

    # Parse corpus
    try:
        dcp = DCParser(fpath_dcorpus)
        dcp.parse_th_mora()
        th_pairs = dcp.th_pair
    except Exception as ex:
        print("Error in parsing dajare corpus", file=sys.stderr)
        raise ex

    ### CONSONANT WPSM MATRIX (WPSM/C)
    print("Calculation of WPSM/C (consonant)", end="\t")

    for mora_tane, mora_henkei in th_pairs:
        ## Perform alignment and append phoneme pairs ##
        pa.set_sequences(mora_tane, mora_henkei, f=lambda m: m.vow)
        pa.do_scoring()
        seq_align, _ = pa.do_traceback()
        for ma_t, ma_h in seq_align:
            if ma_t.vow != ma_h.vow:
                continue
            cofreq_cons[(ma_t.cons, ma_h.cons)] += 1
            freq_cons_t[ma_t.cons] += 1
            freq_cons_h[ma_h.cons] += 1
    wpsm_bldr_cons = WPSMBuilder(cofreq_cons, freq_cons_t, freq_cons_h)
    wpsm_array_cons = wpsm_bldr_cons.calc_wpsm_nSoA()

    # Build and save vocabularies (for T and H) of consonants
    vocab_dic_cons_t, vocab_dic_cons_h = wpsm_bldr_cons.get_vocab()
    vocab_dic_cons_TH = (vocab_dic_cons_t, vocab_dic_cons_h)
    save_serialized(vocab_dic_cons_TH, wpsm_basefname_cons, ".vocab",
            "vocabulary dictionary (for T and H) of consonants")
    #vocab_dic_cons_t, vocab_dic_cons_h = pa.get_vocabdic()

    # Save WPSM/C (separate from vocabularies)
    save_serialized(wpsm_array_cons, wpsm_basefname_cons, ".matrix", "WPSM/C matrix")

    print(wpsm_array_cons)
    print("done")
    ### END WPSM/C

    ### START VOWEL WPSM MATRIX (WPSM/V)
    del(pa)
    pa = PhonNeedlemanWunsch(match=2, mismatch=-1, gap=-1)

    print("Calculation of WPSM/V (vowels)", end="\t")
    THRES_WPSMV = 0.4
    for mora_tane, mora_henkei in th_pairs:
        ## Perform alignment and append phoneme pairs ##
        pa.set_sequences(mora_tane, mora_henkei, f=lambda m: m.cons)
        pa.do_scoring()
        seq_align, _ = pa.do_traceback()
        for ma_t, ma_h in seq_align:
            if ma_t.cons != ma_h.cons:
                try:
                    idx_t_cons = vocab_dic_cons_t[ma_t.cons]
                    idx_h_cons = vocab_dic_cons_h[ma_h.cons]
                    if wpsm_array_cons[idx_t_cons, idx_h_cons] < THRES_WPSMV:
                        continue
                except KeyError:
                    print("KE:", ma_t.cons, ma_h.cons)#d
                    continue
            cofreq_vow[(ma_t.vow, ma_h.vow)] += 1
            freq_vow_t[ma_t.vow] += 1
            freq_vow_h[ma_h.vow] += 1

    wpsm_bldr_vow = WPSMBuilder(cofreq_vow, freq_vow_t, freq_vow_h)
    wpsm_array_vow = wpsm_bldr_vow.calc_wpsm_nSoA()

    # Build and save vocabularies (for T and H) of vowels
    vocab_dic_vow_t = {}
    vocab_dic_vow_h = {}
    for i, vow_t in enumerate(wpsm_bldr_vow.lst_t):
        vocab_dic_vow_t[vow_t] = i
    for j, vow_h in enumerate(wpsm_bldr_vow.lst_h):
        vocab_dic_vow_h[vow_h] = j
    vocab_dic_vow_TH = (vocab_dic_vow_t, vocab_dic_vow_h)
    save_serialized(vocab_dic_vow_TH, wpsm_basefname_vow, ".vocab",
            "vocabulary dictionary (for T and H) of vowels")

    # Save WPSM/V (separate from vocabularies)
    save_serialized(wpsm_array_vow, wpsm_basefname_vow, ".matrix", "WPSM/V matrix")

    print(wpsm_array_vow)
    print("done")
    ### VOWEL WPSM MATRIX (WPSM/V): DONE.


def main(argv=[]):
    """WPSM/C and WPSM/V creation test using Needleman-Wunsch Algorithm.
(Phonetic similarity measure: normalized SOA)
    Outputs:
        Alignment results by Needleman-Wunsch Algorithm.
    Command-line args:
        Arg 0: File path of the Dajare Corpus
        Arg 1: Base file name for WPSM/C
        Arg 2: Base file name for WPSM/V
        """
    if len(argv) < 3:
        print(main.__doc__)
        return 1

    fpath_dcorpus = argv[0]
    wpsm_basefname_cons = argv[1]
    wpsm_basefname_vow = argv[2]

    make_wpsm(fpath_dcorpus, wpsm_basefname_cons, wpsm_basefname_vow)

    print("All done")
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
