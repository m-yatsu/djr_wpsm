# coding: utf-8
"""Feature function objects used for dajare detection.
"""

from collections import defaultdict

import alignment.detect as detect
from alignment.align_operate import PhonNotInVocab

import numpy as np

import phon_engine.oyomi as oyomi

from tokenizer import bag_of_words
import rnnlm.phonlm as phonlm


ENC = "utf-8"

# Function name which begins with 'ftr_' means that it returns a single value.
# With 'ftrG_', a group of features are included in one function object.
# With 'ftrP_', it performs preprocessing for specific feature(s).


def ftrG_bow(text, vocab):
    """GROUP: Bag-of-words features"""
    return bag_of_words(text, vocab)

####                            ####
##   FEATURES FOR DAJARE CORPUS   ##
####                            ####


def ftr_phonsim_c(text, _):
    """Single value of tane-henkei similarity average in given text"""
    sim_a, align_sequences = internal_phonsim_c(text)
    return np.array(sim_a)


def ftrG_phonsim_lmrnn(text, _):
    """TODO: Implement phonlm (Phonetic LM) in order for this feature to work. """
    sim_a, exps_tane, exps_henkei = internal_phonsim_likelihood_c(text)
    exps_tane_u = [u' '.join([m for m in tane]) for tane in exps_tane]
    exps_henkei_u = [u' '.join([m for m in henkei]) for henkei in exps_henkei]
    #print("TANE:", exps_tane_u) #d
    #print("HENKEI:", exps_henkei_u) #d
    if len(exps_tane_u):
        #avglike_tane = sum(phonlm.perplexity_multi(exps_tane_u)) / float(len(exps_tane_u))
        avglike_tane = 0.0 # TODO
        if np.isnan(avglike_tane):
            avglike_tane = 0.0
    else:
        avglike_tane = 0.0
    if len(exps_henkei_u):
        avglike_henkei = sum(phonlm.perplexity_multi(exps_henkei_u)) / float(len(exps_henkei_u))
        if np.isnan(avglike_henkei):
            avglike_henkei = 0.0
    else:
        avglike_henkei = 0.0
    return np.array(sim_a + [avglike_tane, avglike_henkei])


#
# INTERNAL FUNCTIONS (not feature functions)
#

def internal_phonsim_c(text):
    """INTERNAL"""
    #TODO: ここ修正 (Dajare Corpus 対応)
    result = 0.0
    morphs = oyomi.cook(text, get_morphemes=True, as_list=True)
    mora_all = oyomi.cook(text, get_morphemes=False, as_list=True)
    #print(all_mora)#d
    phon_notfound = defaultdict(set)
    offset = 0
    left_seq, right_seq = [], []
    for morph_mora in morphs:
        lenmorph = len(morph_mora)
        mora_left = mora_all[:offset]
        mora_right = mora_all[offset + lenmorph:]
        offset += lenmorph
        if len(morph_mora) < 2:
            continue

        # Values from both side of the tane: leftside and then rightside one
        try:
            left_score, left_seq = detect.align_ss_len(morph_mora, mora_left)
            result += left_score
        except PhonNotInVocab as ePNIV:
            phon_notfound[ePNIV.voc].add(ePNIV.phon)
            continue
        try:
            right_score, right_seq = detect.align_ss_len(morph_mora, mora_right)
            result += right_score
        except PhonNotInVocab as ePNIV:
            phon_notfound[ePNIV.voc].add(ePNIV.phon)
            continue
    return [result], [left_seq, right_seq]


def internal_phonsim_likelihood_c(text, _):
    """INTERNAL: Single value of tane-henkei similarity average in given text"""
    sim_a, align_sequences = internal_phonsim_c(text)
    expressions_t = []
    expressions_h = []
    for align_seq in align_sequences:
        ex_t = []
        ex_h = []
        for t, h in align_seq:
            ex_t.append(t)
            if h:
                ex_h.append(t)
        if len(ex_t):
            expressions_t.append(ex_t)
        if len(ex_h):
            expressions_h.append(ex_h)
    return sim_a, expressions_t, expressions_h
