# coding: utf-8
""" """
import alignment.align_nw as align



ALIGNER = align.Alignment()


def align_SW(seq1, seq2, show_result: bool = False):
    """Does alignment using consonant similarity by default."""
    return ALIGNER.align_mora_sequences(seq1, seq2)


def align_score_sum(seq1, seq2):
    """
    Returns:
        -1.0    -- if no alignment is possible
        average phon similarity of each alignment of moras -- otherwise
    """
    seq_align, scr_align = align.align(seq1, seq2, show_result=False)
    if len(seq_align) == 0:
        return 0.0
    return float(scr_align) / len(seq_align)


def align_ss_len(seq1, seq2):
    seq_align, scr_align = align.align_SW(seq1, seq2, show_result=False)
    if len(seq_align) == 0:
        return 0.0, ()
    return float(scr_align) / len(seq_align), seq_align
