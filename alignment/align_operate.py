# coding: utf-8
"""align_operate.py

Perform alignment of vowels based on
similarity among consonants obtained from WPSM_c.

When run as main program, this script provides
dajare Tane-Henkei detection demonstration utilizing an alignment
algorithm using WPSM_C and WPSM_V as substitution matrices.

Outputs:
    Alignment results by Smith-Waterman Algorithm.
Command-line args:
    Arg 0: Text file with raw dajare text."""

import sys
from typing import Dict, List, Tuple
import numpy as np
#import codecs
#import re
#import math
#import numpy as np
#from collections import defaultdict
from icecream import ic

import phon_engine.oyomi as oyomi
#from phon_engine.mora import *
#from align_nw import PhonNeedlemanWunsch
from my_config import MyConfigParser
import pickle

ENC = "utf-8"
FPATH_INI = "config.ini"
with MyConfigParser(FPATH_INI) as cfg:
    FPATH_WPSM_C = cfg['WPSM']['filename_wpsm_c']
    FPATH_WPSM_C_VOC = cfg['WPSM']['filename_wpsm_c_vocab']
    FPATH_WPSM_V = cfg['WPSM']['filename_wpsm_v']
    FPATH_WPSM_V_VOC = cfg['WPSM']['filename_wpsm_v_vocab']
print(FPATH_WPSM_C, FPATH_WPSM_C_VOC, FPATH_WPSM_V, FPATH_WPSM_V_VOC)#d


class PhonNotInVocab(Exception):
    pass


class THRule(object):
    def __init__(self) -> None:
        # Load WPSM/C
        try:
            with open(FPATH_WPSM_C, "r") as fpr:
                self.npmi_cons_array = pickle.load(fpr)
        except Exception as ex:
            print("Error loading from {0}: {1}".format(FPATH_WPSM_C, ex),
                  file=sys.stderr)
            raise
        # Load WPSM/V
        try:
            with open(FPATH_WPSM_V, "r") as fpr:
                self.npmi_vow_array = pickle.load(fpr)
        except Exception as ex:
            print("Error loading from {0}: {1}".format(FPATH_WPSM_V, ex),
                  file=sys.stderr)
            raise

    @staticmethod
    def detect(sent: str) -> List[Tuple]:
        """Reads a sentence.

        Args:
            (unicode) sent: input sentence.
        Returns:
            A list including detection results."""

        # Moratize and do alignment
        moratized_morph_all = oyomi.cook(sent, get_morphemes=True, as_list=True)

        # Calculate score and print
        ic(moratized_morph_all)#d
        for i, morph in enumerate(moratized_morph_all):
            print("{0:2d}   {1}".format(i, morph), file=sys.stderr)#d

        return []


def lookup_wpsm(wpsm: np.array,
                dic0: Dict, dic1: Dict,
                phon0, phon1):
    try:
        phon0_idx, phon1_idx = dic0[phon0], dic1[phon1]
    except KeyError:
        raise PhonNotInVocab
    if phon0_idx > phon1_idx:
        return wpsm[phon1_idx, phon0_idx]
    else:
        return wpsm[phon0_idx, phon1_idx]


def main(argv=[]):
    """For trying from console."""
    if len(argv) < 0:
        print(main.__doc__, file=sys.stderr)
        return 1

    thr = THRule()
    quit = False
    while quit is False:
        ipt = input(u"> ").decode(ENC)
        if ipt == u'q':
            quit = True
            continue
        moratized_morph_all = thr.detect(ipt)
        print(moratized_morph_all)

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
