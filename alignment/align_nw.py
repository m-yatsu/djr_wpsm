# coding: utf-8
"""Algorithm implementations for mora alignment.

The Needleman-Wunsch algorithm consists of three steps:

1. [set_sequences] Initialization of the score matrix
2. [calc_score] Calculation of scores and filling the traceback matrix
3. [get_alignment] Deducing the alignment from the traceback matrix

cf. https://www.cs.sjsu.edu/~aid/cs152/NeedlemanWunsch.pdf
"""

import configparser
import pickle
import pprint  # debug
import sys
from typing import Any, Callable, Iterable, List, Tuple

import numpy as np

from phon_engine.mora import DummyMora, Mora
pp = pprint.PrettyPrinter(indent=4)  # debug
cp = configparser.SafeConfigParser()
cp.read("config.ini")

# Traceback scores
TB_DONE, TB_DIAG, TB_UP, TB_LEFT, TB_NA = 0, 1, 2, 3, -1

# Locations for dumping WPSMs
BASE_DIR = cp['WPSM']['base_dir'] #"/Users/my/ex/lm/wpsm_soa/"
FPATH_WPSMC = BASE_DIR + cp['WPSM']['filename_wpsm_c']
FPATH_WPSMV = BASE_DIR + cp['WPSM']['filename_wpsm_v']
FPATH_WPSMC_VOC = BASE_DIR + cp['WPSM']['filename_wpsm_c_vocab']
FPATH_WPSMV_VOC = BASE_DIR + cp['WPSM']['filename_wpsm_v_vocab']


class AlignmentFault(Exception):
    '''Alignment Failed'''
    pass


class TracebackFault(AlignmentFault):
    '''Traceback failed'''
    pass


class ScoringFault(AlignmentFault):
    '''Scoring failed'''
    pass


def Zd() -> float:
    return 0.0


def max_arg(*seq) -> Any:
    m = max(seq)
    i = seq.index(m)
    return m, i


def mat_nonzero(array: np.ndarray):
    return np.shape(np.nonzero(array))[1]


def argmax2d(arr2d: np.ndarray) -> Tuple[int, int]:
    """
    Args:
        Argmax of two-dimensional NumPy array.
    Returns:
        (int, int) The [i, j] indices of the item with the maximum value."""
    ilen, jlen = arr2d.shape
    imax, jmax = int((1 + arr2d.argmax()) / jlen), int((1 + arr2d.argmax()) % jlen - 1)
    return imax, jmax


class PhonNeedlemanWunsch(object):

    def __init__(self, match=2, mismatch=-1, gap=-2):
        self.sc_match = match
        self.sc_misma = mismatch
        self.sc_gap = gap

        self.seq_1 = None
        self.seq_2 = None
        self.objseq_1 = []
        self.objseq_2 = []
        self.vocabdict_1 = {}
        self.vocabdict_2 = {}

        self.mat_score = None
        self.mat_trace = None
        self.mat_subst = None

        self.n_rows = 0
        self.n_cols = 0
        #shape = (0, 0)

    def set_sequences(self,
                      objseq_1: Iterable, objseq_2: Iterable,
                      f: Callable = lambda x: x,
                      mat_subst: np.ndarray = None) -> None:
        """Initialize the score matrix and the traceback matrix.
        Must be run first!
        Set the two sequences to be aligned, and then initialize the internal matrices.
        Sequences are preprocessed using given function f() (default is identity).
        A substitution matrix can be specified as the 4th argument."""
        self.objseq_1 = objseq_1
        self.objseq_2 = objseq_2
        self.seq_1 = [f(itm) for itm in objseq_1]
        self.seq_2 = [f(itm) for itm in objseq_2]
        self.n_rows, self.n_cols = len(self.seq_1) + 1, len(self.seq_2) + 1
        _shape = self.n_rows, self.n_cols

        # Substitution matrix
        if isinstance(mat_subst, np.ndarray) and mat_nonzero(mat_subst):
            self.mat_subst = mat_subst
        else:
            print("set_sequences(): The given substitute matrix"
                  " is not a NumPy array! SubMat initiallized.", file=sys.stderr)#d
            self.init_mat_subst()

        # Scoring matrix
        self.mat_score = np.zeros(_shape)
        for i in range(1, self.n_rows):
            self.mat_score[i][0] = self.mat_score[i - 1][0] + self.sc_gap
        for j in range(1, self.n_cols):
            self.mat_score[0, j] = self.mat_score[0][j - 1] + self.sc_gap

        # Traceback matrix
        self.mat_trace = np.full(_shape, None)
        for i in range(1, self.n_rows):
            self.mat_trace[i, 0] = TB_UP
        for j in range(1, self.n_cols):
            self.mat_trace[0, j] = TB_LEFT

    def init_vocab(self):
        """TODO: Initialize vocabulary dictionaries. To be called in case
        substitution matrix is given externally."""

    def init_mat_subst(self):
        """Create substitution matrix only from given sequences.
        Does not ."""
        assert self.seq_1 and self.seq_2, "Sequences not set!"
        vocab_1, vocab_2 = set(self.seq_1), set(self.seq_2)
        vocab_common = vocab_1 & vocab_2
        vocab_1only = vocab_1 - vocab_common
        vocab_2only = vocab_2 - vocab_common
        _shape = len(vocab_1), len(vocab_2)

        self.mat_subst = np.zeros(_shape)

        for i, x1 in enumerate(sorted(list(vocab_common))):
            for j, x2 in enumerate(sorted(list(vocab_common))):
                self.mat_subst[i, j] = self.sc_match if i == j else self.sc_misma
        for i in range(len(vocab_common), len(vocab_common) + len(vocab_1only)):
            for j in range(len(vocab_2)):
                self.mat_subst[i, j] = self.sc_misma
        for i in range(len(vocab_1)):
            for j in range(len(vocab_common), len(vocab_common) + len(vocab_2only)):
                self.mat_subst[i, j] = self.sc_misma

        vocab_sorted1: list = sorted(list(vocab_common)) + sorted(list(vocab_1only))
        vocab_sorted2: list = sorted(list(vocab_common)) + sorted(list(vocab_2only))
        # Substitution matrix inv. dict.: phoneme -> index #
        for i, phon in enumerate(vocab_sorted1):
            self.vocabdict_1[phon] = i
        for j, phon in enumerate(vocab_sorted2):
            self.vocabdict_2[phon] = j

    def get_mat_subst(self):
        return self.mat_subst

    def reset_mat_subst(self):
        self.mat_subst = None

    def vocabindex_1(self, c1):
        assert self.vocabdict_1, 'Vocab. Dictionary for Seq 1 is empty!'
        try:
            return self.vocabdict_1[c1]
        except KeyError:
            print("Seq 1", self.seq_1, file=sys.stderr)
            print("Seq 2", self.seq_2, file=sys.stderr)
            print("KeyErr", c1, self.vocabdict_1, self.vocabdict_2, file=sys.stderr)
            return 0 # If not found, treat them as non-existent sounds

    def vocabindex_2(self, c2):
        assert self.vocabdict_2, 'Vocab. Dictionary for Seq 2 is empty!'
        try:
            return self.vocabdict_2[c2]
        except KeyError:
            print("Seq 1", self.seq_1, file=sys.stderr)
            print("Seq 2", self.seq_2, file=sys.stderr)
            print("KeyErr", c2, self.vocabdict_1, self.vocabdict_2, file=sys.stderr)
            return 0 # If not found, treat them as non-existent sounds

    def get_vocabdicts(self):
        return self.vocabdict_1, self.vocabdict_2

    def set_vocabdict_1(self, dic):
        self.vocabdict_1 = dic

    def set_vocabdict_2(self, dic):
        self.vocabdict_2 = dic

    def set_vocabdicts(self, dic1, dic2):
        self.set_vocabdict_1(dic1)
        self.set_vocabdict_2(dic2)


    def do_scoring(self):
        """Scoring (Needleman-Wunsch method).

        Calculate scores and then fill the traceback matrix.
        Sequences and directions:
            seq1 --> in rows, seq2 --> in columns of the matrices"""
        for mat, matname in ((self.mat_score, 'Score'),
                             (self.mat_trace, 'Trace'),
                             (self.mat_subst, 'Subst')):
            if not mat_nonzero(mat):
                raise ScoringFault(f"NW: {matname} matrix not available!")
        # Search for scores
        for i, p1 in enumerate(self.seq_1, start=1):
            for j, p2 in enumerate(self.seq_2, start=1):
                try:
                    q_diag = self.mat_score[i - 1, j - 1] + \
                        self.mat_subst[self.vocabindex_1(p1), self.vocabindex_2(p2)]
                except Exception as e:
                    msg = (f"Failed to accumlate score({self.vocabindex_1(p1)},"
                           f" {self.vocabindex_2(p2)})"
                           f" reason: {repr(e)}")
                    raise ScoringFault(f"No score for ('{p1}', '{p2}'); {msg}")
                q_up = self.mat_score[i - 1, j] + self.sc_gap
                q_left = self.mat_score[i, j - 1] + self.sc_gap
                self.mat_score[i, j], line = max_arg(q_diag, q_up, q_left)
                self.mat_trace[i, j] = (TB_DIAG, TB_UP, TB_LEFT)[line]


    def do_traceback(self) -> Tuple[List[Tuple[Mora, Mora]],  # Alignment result (sequence)
                                    float]: # Alignment result (score sum)
        """Traceback (Needleman-Wunsch method).
        Deduce the alignment from the traceback matrix.
        """
        seq_align = []
        scr_align = 0.0
        i, j = self.n_rows - 1, self.n_cols - 1
        #i_prev, j_prev = None, None
        while i > 0 and j > 0:
            tr = self.mat_trace[i, j]
            if tr == TB_DIAG:   # Match
                mora_1 = self.objseq_1[i - 1]
                mora_2 = self.objseq_2[j - 1]
                i_delta, j_delta = 1, 1
            elif tr == TB_UP:   # Gap
                mora_1 = self.objseq_1[i - 1]
                mora_2 = DummyMora()
                i_delta, j_delta = 1, 0
            elif tr == TB_LEFT:  # Gap
                mora_1 = DummyMora()
                mora_2 = self.objseq_2[j - 1]
                i_delta, j_delta = 0, 1
            else:
                raise TracebackFault(f"Invalid traceback direction value: {tr}\n"
                                     f"Expected: {TB_DIAG}(DIAG), {TB_UP}(UP), or {TB_LEFT}(LEFT)")

            seq_align.append((mora_1, mora_2))
            #i_prev, j_prev = i, j
            i, j = i - i_delta, j - j_delta
            scr_align += self.mat_score[i, j]
        return seq_align, scr_align


class PhonSmithWaterman(PhonNeedlemanWunsch):
    """Do alignment using Smith-Waterman algorithm.
    Applicable to any alignable data by giving function
    which returns target kind of element to align."""

    # Scoring (Smith-Waterman)
    #   Forces all the scores to be non-negative.
    def do_scoring(self) -> None:
        """Scoring (Smith-Waterman)"""
        for mat, matname in ((self.mat_score, 'Score'),
                             (self.mat_trace, 'Trace'),
                             (self.mat_subst, 'Subst')):
            if not mat_nonzero(mat):
                raise ScoringFault(f"NW: {matname} matrix not available!")

        # Search for scores
        p1: Mora
        p2: Mora
        for i, p1 in enumerate(self.seq_1, start=1):
            for j, p2 in enumerate(self.seq_2, start=1):
                q_diag = self.mat_score[i - 1, j - 1] \
                    + self.mat_subst[self.vocabindex_1(p1), self.vocabindex_2(p2)]
                q_up = self.mat_score[i - 1, j] + self.sc_gap
                q_left = self.mat_score[i, j - 1] + self.sc_gap
                self.mat_score[i, j], q = max_arg(0, q_diag, q_up, q_left)
                self.mat_trace[i, j] = (TB_DONE, TB_DIAG, TB_UP, TB_LEFT)[q]

    # In S-W algorithm, Traceback begins with the cell with the
    # highest score in Score Matrix.
    def do_traceback(self) -> Tuple[List[Tuple[Mora, Mora]], float]:
        """ Traceback (Smith-Waterman) """

        seq_align = []
        scr_align = 0.0
        i, j = argmax2d(self.mat_score)

        while i > 0 and j > 0:
            tr = self.mat_trace[i, j]
            if tr == TB_DONE:   # Termination
                break
            elif tr == TB_DIAG:  # Match
                mora_1 = self.objseq_1[i - 1]
                mora_2 = self.objseq_2[j - 1]
                i_delta, j_delta = 1, 1
            elif tr == TB_UP:   # Gap
                mora_1 = self.objseq_1[i - 1]
                mora_2 = DummyMora()
                i_delta, j_delta = 1, 0
            elif tr == TB_LEFT:  # Gap
                mora_1 = DummyMora()
                mora_2 = self.objseq_2[j - 1]
                i_delta, j_delta = 0, 1
            else:
                raise TracebackFault('Trace value invalid or not given')
            seq_align.append((mora_1, mora_2))
            #i_prev, j_prev = i, j
            i, j = i - i_delta, j - j_delta
            scr_align += self.mat_score[i, j]

        return seq_align, scr_align
    # end method do_traceback()
# end class PhonSmithWaterman


def show_alignresult(
        seq_1: List[Mora],
        seq_2: List[Mora],
        seq_align: List[Tuple[Mora, Mora]],  # alignment done
        width: int = 3) -> None:
    """Show result of alignment"""
    _fmt = u"{:%ss}" % width

    print("### Mora sequences to be examined:")
    for mora in seq_1:
        print(_fmt.format(str(mora)), end=" ")
    print()
    for mora in seq_2:
        print(_fmt.format(str(mora)), end=" ")
    print()
    print()

    print("### Mora alignment result (length {0}):".format(len(seq_align)))
    as1, as2 = u'', u''
    for am1, am2 in seq_align:
        as1 = _fmt.format(str(am1)) + u' ' + as1
        as2 = _fmt.format(str(am2)) + u' ' + as2
    print(as1)
    print(as2)


def f_cons(m: Mora) -> str:
    """子音部分抽出関数；抽出プロセスに変更ある場合はここに定義"""
    return m.cons


def f_vow(m: Mora) -> str:
    """母音部分抽出関数；抽出プロセスに変更ある場合はここに定義"""
    return m.vow


class Alignment(object):
    """Class for alignment of actual inputs.
    Both consonant and vowel WPSMs must be loadable."""
    def __init__(self,
                 method: str = 'sw',
                 match: int = 2,
                 mismatch: int = -1,
                 gap: int = -0.3) -> None:
        try:
            with open(FPATH_WPSMC, "rb") as f_wpsmc_read:
                self.wpsm_c = pickle.load(f_wpsmc_read)
            with open(FPATH_WPSMV, "rb") as f_wpsmv_read:
                self.wpsm_v = pickle.load(f_wpsmv_read)
        except IOError as e:
            raise e
        self.wpsm_c_voc1, self.wpsm_c_voc2 = pickle.load(open(FPATH_WPSMC_VOC, "rb"))
        self.wpsm_v_voc1, self.wpsm_v_voc2 = pickle.load(open(FPATH_WPSMV_VOC, "rb"))
        # Alignment DP algorithm
        if method == 'sw':
            self.pa = PhonSmithWaterman(match, mismatch, gap)
        else:
            self.pa = PhonNeedlemanWunsch(match, mismatch, gap)

    def align_mora_sequences(self,
                             seq_1: List[Mora],
                             seq_2: List[Mora],
                             cv: str = 'c',
                             show_result: bool = False) -> Tuple[List[Mora], float]:
        """Perform alignment and returns aligned (paired) sequence and the sum of alignment scores.
        Alignment scores are gained using consonant similarity from WPSMc by default."""
        if cv == 'c':
            func = f_cons
            wpsm = self.wpsm_c
            wpsm_vocab_1, wpsm_vocab_2 = self.wpsm_c_voc1, self.wpsm_c_voc2
        else:
            func = f_vow
            wpsm = self.wpsm_v
            wpsm_vocab_1, wpsm_vocab_2 = self.wpsm_v_voc1, self.wpsm_v_voc2
        self.pa.set_sequences(seq_1, seq_2, f=func, mat_subst=wpsm)
        self.pa.set_vocabdicts(wpsm_vocab_1, wpsm_vocab_2)
        self.pa.do_scoring()
        seq_align, scr_align = self.pa.do_traceback()

        if show_result:
            show_alignresult(seq_1, seq_2, seq_align)

        return seq_align, scr_align
