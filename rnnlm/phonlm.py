#!/usr/bin/env python
# coding: utf-8
""" """

import codecs
import subprocess

ENC = "utf-8"
RNNLM_DIR = "/home/my/ex/lm/rnnlmphon"
SCR_FAIL = 500.0


def perplexity(text):
    # Write text to tmp file
    with codecs.open(RNNLM_DIR + "/tst.txt", "w", encoding=ENC) as fpw:
        fpw.write(text)
    # Run command
    cmd = "{0}/rnnlm -rnnlm {0}/phonmodel -test {0}/tst.txt".format(RNNLM_DIR)
    cmdA = cmd.split()
    try:
        ppl_s = subprocess.check_output(cmdA)
        ret = float(ppl_s.strip())
    except subprocess.CalledProcessError:
        ret = SCR_FAIL
    except Exception:
        raise
    return ret


def perplexity_multi(texts):
    #print(texts)#d
    return [perplexity(txt) for txt in texts]
