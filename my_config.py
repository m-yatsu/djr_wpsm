#!/usr/bin/env python
# coding: utf-8
"""Checks config.ini file for """

from __future__ import print_function
import sys
import os
if sys.version_info < (3, ):
    import ConfigParser as configparser
else:
    import configparser

ENC = "utf-8"
FPATH_INI = "config.ini"

class MyConfigParser(configparser.ConfigParser):
    def __enter__(self):
        return self

    def __init__(self, fpath_ini):
        super(configparser.ConfigParser, self).__init__(
                interpolation=configparser.ExtendedInterpolation())
        if os.path.exists(fpath_ini):
            self.read(fpath_ini)
            self.ini_path = fpath_ini
        else:
            sys.stderr.write("Not found: " + fpath_ini + "\n")

    def __exit__(self, ex_type, ex_value, trace):
        self.clear()
        del(self.ini_path)
        del(self)

def print_config(config, section, items=[]):
    print(section + ":")
    if section not in config:
        print("Not found: " + section)
        return
    itemList = items if items != [] else config[section].keys()
    for item in itemList:
        print(" - {}: {}".format(item, config[section][item]))
    print("")

def print_config_all(config):
    print("# {}\n".format(config.ini_path))
    for section in config.keys():
        print("## {}:".format(section))
        for item in config[section]:
            print(" - {}: {}".format(item, config[section][item]))
        print("")

def main(argv):
    """Main"""

    # Load
    with MyConfigParser(FPATH_INI) as config:
        # Make sure all parameters are set properly
        #print_config(config, 'Tools', ['mecabrc_path', 'jumanserver_port'])
        print_config_all(config)
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
