# coding: utf-8
"""Pythonモジュールレベル及びFlaskによるAPI"""

"""初期化"""


def preprocess():
    """入力文に対する前処理．
     * 文字ベースでの前処理
     * 形態素解析
     * 読み付与
     * Mora 化
     * 形態素(種表現候補)単位での音韻処理
    利用:
        - preprocessing.Preprocess"""


def detect():
    """検出試行 (1事例)．
    利用:
        alignment.align_operate"""


def detect_from_file():
    """検出試行 (DC形式ファイル)"""


def stat_corpus():
    """DC形式ファイルの統計"""


def update_wpsm():
    """WPSMの更新"""


def train_djre():
    """分類器の訓練"""
    pass

"""駄洒落がすべての人々を幸福にする"""
