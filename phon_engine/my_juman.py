#coding: utf-8

from __future__ import print_function
import shlex
import sys
from subprocess import PIPE, Popen

ENC = 'utf-8'

# TODO: JUMAN の起動確認＋自動的に起動


class MyJuman(object):

    def __init__(self, jumanpath="/usr/local/bin/juman"):
        self.jumanpath = jumanpath

    def juman_socket(self, name="localhost", port=32082, option=""):
        cmd_s = "%s -C %s:%s %s" % (self.jumanpath, name, port, option, )
        cmd = shlex.split(cmd_s)
        try:
            self.process = Popen(cmd, bufsize=4096, shell=True, stdin=PIPE,
                                 stdout=PIPE)
        except Exception as e:
            print("Err", e)
            sys.exit()
        #print("juman_socket(): success")#d

    def post_juman(self, context):
        before_eos = True
        self.process.stdin.write((context + "\n").encode(ENC))
        self.process.stdin.flush()

        while before_eos:
            line = self.process.stdout.readline().decode(ENC)
            yield line
            if line == "EOS\n":
                before_eos = False

    def juman_close(self):
        self.process.stdin.close()
        wait = self.process.wait()
        if wait != 0:
            print("There were some errors", wait)
