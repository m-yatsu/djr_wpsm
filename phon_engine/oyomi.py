#!/usr/bin/env python
#coding: utf-8
"""Dajare detection based on sounds.
The threshold specified is the Levenstein distance of
the sequence of consonants after normalising vowels"""

# 1文からの検出は kenshutu_1()
# JUMAN または MeCab で -Oyomi させる．出力は平仮名．

# 素性タイプは 343 行目で指定せよ (種類は 168 行目)

from __future__ import print_function
import codecs
import re
import sys
from datetime import datetime
from typing import List, Union

import phon_engine.chouon_process as cp
import phon_engine.convert_mora as cm
import phon_engine.keitaiso_kaiseki as kk
import phon_engine.phon_similarity_2 as ps
import phon_engine.tables as tables # カナかな変換表, A〜O段発音対応表
import phon_engine.th_detect as td
from phon_engine.mora import Mora, NonPhonemicMora
from phon_engine.th_detect_common import Tr

import regex

from typing import Callable
from icecream import ic
ic("IceCream enabled in oyomi.py")

ENC = "utf-8"
kht = tables.kht
aht = tables.aht
td0 = td.THDetectorNew()

##
##
## SUB
##
##

TRANS_NUMBER = {ord(u'0'): u'〇',
                ord(u'1'): u'一',
                ord(u'2'): u'二',
                ord(u'3'): u'三',
                ord(u'4'): u'四',
                ord(u'5'): u'五',
                ord(u'6'): u'六',
                ord(u'7'): u'七',
                ord(u'8'): u'八',
                ord(u'9'): u'九',
                ord(u'０'): u'〇',
                ord(u'１'): u'一',
                ord(u'２'): u'二',
                ord(u'３'): u'三',
                ord(u'４'): u'四',
                ord(u'５'): u'五',
                ord(u'６'): u'六',
                ord(u'７'): u'七',
                ord(u'８'): u'八',
                ord(u'９'): u'九'}


def trans_number(str_u):
    return str_u.translate(TRANS_NUMBER)


def kataeiji2hira(textU):
    for char_kata, char_hira in tables.kht.items():
        textU = textU.replace(char_kata, char_hira)
    for char_eiji, trans_kata in tables.aht.items():
        #ic(trans_kata)
        #ic(trans_kata[0])
        #ic(tables.alphabet_trans[trans_kata[0]])
        #ic(len(tables.alphabet_trans))
        after = tables.alphabet_trans[trans_kata[0]]
        textU = textU.replace(char_eiji, after)
    textU = regex.sub(r"\p{Han}+", "", textU)
    return textU


def print_out(resultG):
    for line in resultG:
        print(line)


def print_out0(resultG):
    for line in resultG:
        print(line[0].encode(ENC))


def print_out_left_length(resultG):
    for line in resultG:
        c, raw = line
        c2A = c.replace("|| ", "").split(" ")
        print(len(c2A))

#####################
#    文内検出 1      #
#####################

#
#
# PER LINE
# 読み，モーラ化（1文）
#
#


def k1_has_euphonic(cooked):
    """モーラ表記入力文に促撥長音[N, Q, -]が含まれるか否かを返す

    Param:
        cooked -- 入力文 (調理済, unicode)"""
    lst = (u'N', u'Q', u'-', # 長音は大抵，既に下記へ置換されている
           u'a || a',
           u'i || i',
           u'u || u',
           u'e || e',
           u'o || o')
    result = any([m in cooked for m in lst])
    return result


def k1_cooker_line_kana(djr, ma="m", noephn=0):
    # unicode化，カッコ括弧等の除去
    #djr = Tr.pre2(Tr.kakko(djr.decode(ENC)))
    try:
        djr = Tr.kakko(djr)
    except Exception as e:
        print("ERROR:", djr)
        raise e
    djr = Tr.pre2(djr)
    djr_cooked = djr
    if ma == "j":
        pass
    elif ma == "m":
        # MeCab が最初
        l_w, l_p = kk.get_yomi_hinshi(djr)
        l_wh = kataeiji2hira(l_w).strip()
        djr_cooked = l_wh
    return djr_cooked, djr

re_num = re.compile(r'^[0-9]$')
re_nums = re.compile(r'^[0-9.,][0-9.,]+$')
re_commas = re.compile(r'^[.,]+$')
re_ascii = re.compile(r'^[\x00-\x7f]+$')


def is_ja_string(string):
    """Checks if given string has any Japanese characters."""
    return not re_ascii.match(string)


def postprocess(mora: Mora) -> Mora:
    """Changes some mora's srface."""
    if type(mora) is NonPhonemicMora:
        srf = mora.srface
        srf_ret = srf
        if re_nums.match(srf) and not re_commas.match(srf):
            srf_ret = u'00'
        elif re_num.match(srf):
            srf_ret = u'0'
        else:
            srf_ret = u'XX'
        mora.set_surface(srf_ret)
    return mora


def to_list2d(lst_mora: List[Mora]) -> List[List[Mora]]:
    """Converts list of mora with delimiter to a 2-d list."""
    ret: List[List[Mora]] = []
    added: List[Mora] = []
    for mora in lst_mora:
        if mora.is_delimiter:
            if added:
                ret.append(added)
                added = []
            continue
        added.append(mora)
    if added:
        ret.append(added)
    return ret


def exists_yomi(mora: Mora):
    return not mora.is_delimiter and not mora.is_nonmora


def partof_sent(mora: Mora):
    return mora.is_delimiter or not mora.is_nonmora


def cook(line: str,
         #ma: str = "m",
         #noephn: bool = False,
         get_morphemes: bool = True,
         as_list: bool = False,
         as_str: bool = False,
         yomi_is_given: bool = False,
         proc_func: Callable = postprocess) -> Union[List[List[Union[Mora, str]]], List[Union[Mora, str]], str]:
    """
    前処理
    子音/母音比較を行えるように，駄洒落文字列djrをMora列へ変換(調理)する．
    流れ: djr=>MeCab読み=>Kakasi読み=>平仮名化=>モーラ化=>長音処理

    入力:
        (unicode) line: Input raw string.
    返り値:
        list of Mora objects
            OR list of list of Mora objects
            OR string

    >>> cook("吹田市で髪を梳いたし")  # -> List[Mora]
    # get_morphemes, as_list, as_str =
    #     True, False, False
    [/su/, /i/, /ta/, ||, /Si/, ||, /de/, ||, /ka/, /mi/, ||, /o/, ||, /su/, /i/, ||, /ta/, ||, /Si/]
    >>> cook("吹田市で髪を梳いたし", as_list=True)  # -> List[List[Mora]]
    #     True, True, False
    [[/su/, /i/, /ta/], [/Si/], [/de/], [/ka/, /mi/], [/o/], [/su/, /i/], [/ta/], [/Si/]]
    >>> cook("吹田市で髪を梳いたし", as_list=True, as_str=True)  # -> List[str]
    #     True, True, True
    ['su i ta', 'Si', 'de', 'ka mi', 'o', 'su i', 'ta', 'Si']
    >>> cook("吹田市で髪を梳いたし", as_str=True)  # -> str
    #     True, False, True
    'su i ta || Si || de || ka mi || o || su i || ta || Si'
    >>> cook("吹田市で髪を梳いたし", as_str=True, get_morphemes=False)  # -> str
    #     False, False, True
    'su i ta Si de ka mi o su i ta Si'
    """
    #
    #　 / ,,/
    #　(；`・ω・）｡･ﾟ･⌒）
    #　/　　 ｏ━ヽニニフ))
    #　しー-Ｊ
    # 日本語文字列がある場合のみ処理
    _empty = [] if get_morphemes else ''
    #if line == "" or not is_ja_string(line):
    if not line:
        return _empty
    ic("LINE NOT empty")
    # unicode化，カッコ括弧等の除去
    #djr = re_num.sub(u'0', Tr.pre2(Tr.kakko(djr)))
    l_t = trans_number(line)
    # 読み(カタカナ) & 形態素境界の獲得
    if not yomi_is_given:
        l_tw, l_tp = kk.get_yomi_hinshi(l_t)
    else:
        l_tw, l_tp = re.sub(r"\s+", " || ", l_t), [None] * len(l_t.split())
    ic(line, l_tw)
    # "助詞係助詞" の品詞タグのある "ハ" を "ワ" に置換
    l_tw2 = kk.convert_pp(l_tw, l_tp)
    ic(l_tw2)
    # カタカナ・英字を変換し全てをひらがなへ
    l_twh = kataeiji2hira(l_tw2).strip()
    if l_twh == u'':
        ic("L_TWH EMPTY")
        return _empty
    ic("L_TWH NOT empty", l_twh)
    # Mora 列化
    l_twhm: List[Mora] = cm.moratize(l_twh)
    #ic(l_twhm)
    # 長音 Mora の二重母音化 (形態素境界を考慮)
    l_twhmc: List[Mora] = cp.chouon_obj(l_twhm)
    #ic(l_twhmc)

    # 結果の返却
    if get_morphemes:
        if as_list:
            l2d = to_list2d([proc_func(m) for m in l_twhmc])
            if as_str: # 形態素毎に文字列化したリスト True, True, True
                return [" ".join(proc_func(m).surface for m in morph if partof_sent(m)) for morph in l2d]
            return l2d # 形態素毎にリスト化した2次元リスト True, True, False
        if as_str:
            return " ".join(proc_func(m).surface for m in l_twhmc)  # True, False, True
        return [proc_func(m) for m in l_twhmc] # DelimiterMora を保持した平坦リスト True, False, False
    if as_str:
        if as_list:  # False, True, True
            return [proc_func(m).surface for m in l_twhmc if exists_yomi(m)]  # 形態素境界を無視した文字列リスト
        # False, False, True
        return " ".join(proc_func(m).surface for m in l_twhmc if exists_yomi(m)) # 形態素境界を無視した単一文字列
    # False, True, False OR False, False, False
    return [proc_func(m) for m in l_twhmc if exists_yomi(m)] # 形態素境界を無視した平坦リスト


# Delete!
def k1_cooker_line(djr, ma="m", euphonic=True):
    """
    子音・母音比較を行えるように，駄洒落文字列djrを調理する．
    a. djr.SayKotoeri2読み.MeCab読み.Kakasi読み.平仮名化.モーラ化
    b. djr.JUMAN読み分かち.MeCab読み.Kakasi読み.平仮名化.モーラ化
    現在使用中: bｓ

    (読みだけ，読み分かち) を返す <-- 現在停止
    読み分かち(unicode) を返す
    入力:
        djr (utf-8 string)
        ma (utf-8 string) -- 最初に使用する形態素解析器
    返り値:
        djr_cooked (Unicode string)
        djr (Unicode string)
    """
    # unicode化，カッコ括弧等の除去
    djr = Tr.pre2(Tr.kakko(djr))
    if ma == "j":
        # JUMAN が最初 <NOT CODED YET!>
        l_w = kk.oyomi_wakati_juman_line(djr)
    elif ma == "m":
        # MeCab が最初 (JUMAN 不使用)
        #l_w, l_p = kk.oyomi_wakati_mecab_line(djr)
        l_w, l_p = kk.get_yomi_hinshi(djr)
        #print(djr, l_w)#d
        l_wh = kataeiji2hira(l_w).strip()
        #print(l_wh)#d3
        if not euphonic:
            l_wh = ps.supress_euphonic(l_wh)
        l_whm = cm.moratize(l_wh)
        #l_whmc = cp.chouon_new(l_whm)
        djr_cooked = l_whm
        #djr_cooked = l_whmc
    return djr_cooked, djr

#
#  検出（1文）
#  検出アルゴリズム(のmain)はここに書く
#  対話システムが実際に用いるのもコレ
#


# Delete!
def kenshutsu_1(djr_cooked, djr_raw, val=0.05, ft_type=1):
    """同一音素列の中で，形態素区切をまたがない音素列，及び
    またぐ音素列，それぞれの一致を検出する．

    入力:
        djr_cooked: モーラ化した形態素境界付きの文"""

    # ls_tane: 形態素毎の音素列を得る
    # ls_henk: 全音素列を得る (形態素境界無し)
    # 併置型駄洒落の条件は，文内に類似音素列が2つ存在すること
    ## またがない m 形態素境界有り
    ## またぐ w
    # 現在はまたぐもののみ mode w: 形態素境界無視

    #print("djr_cooked:", djr_cooked)#d

    scores = [0.0]

    if ft_type == 1:
        val = 0.01  # 完全一致
        result_whmce = td0.findTane_kana(djr_cooked, "w", val)
    elif ft_type == 2:
        val = 0.28  # 類似度
        #result_whmce = td0.findTane2(djr_cooked, "w", val)
        result_whmce, scores = td0.findTane2withScore(djr_cooked, "w", val)
    elif ft_type == 3:
        val = 0.99  # 促撥長有り類似度
        cooked2 = kk.owakati_mecab_line(djr_raw)
        result_whmce = td0.findTane2insert(djr_cooked, cooked2, "w", val)

    return result_whmce, scores


def kenshutsu_1k(djr_cooked, val=0.05):
    """同一音素列の中で，形態素区切をまたがない音素列，及び
    またぐ音素列，それぞれの一致を検出する．

    入力:
        djr_cooked: モーラ化した形態素境界付きの文"""

    result_whmce = td0.findTane_kana(djr_cooked, "w", val)

    return result_whmce


#
#
# PER FILE
# ファイル毎の処理
#
#


#
# 検出（ファイル毎）: 検出した種・変形のタプルを返す
#


def detect_tofile(generator, val=0.05, fname_p="FP", fname_n="TN", ft_type=1):
    """ジェネレータ generator から供給される，
       cooked: 形態素境界付きモーラ列と，raw: 生の駄洒落文字列から
       検出を行う．
       検出した文を，ファイル名の末尾が {fname_p} のファイルに，
       しなかった文は {fname_n} のファイルに書き込む．

    Params:
        generator -- テキストを受け取り，Mora列及び文字列を返すジェネレータ
        val -- しきい値
        fname_p -- 検出例保存先ファイル名 (陽性)
        fname_n -- 検出例保存先ファイル名 (陰性)
    Retrns:
        len(detect_p.keys()) -- 陽性検出数
        length -- 処理済み文総数"""
    detect_p = {}
    detect_n = {}

    # line by line
    fp_p = codecs.open("detected_%s.txt" % (fname_p, ), "w", encoding=ENC)
    fp_n = codecs.open("detected_%s.txt" % (fname_n, ), "w", encoding=ENC)
    for i, (cooked, raw) in enumerate(generator):
        # 類似音素列の存在 (文内)
        print("{:3d}:".format(i + 1),)
        k1, scores = kenshutsu_1(cooked, raw, val, ft_type=ft_type)
        if k1:
            detect_p[cooked] = k1
            fp_p.write(cooked)
            continue
        # 類似音素列の存在 (文外)
        k2 = kenshutsu_2(cooked)
        if k2:
            detect_p[cooked] = k2
            fp_p.write(cooked)
            continue
        # 不検出および例外
        detect_n[cooked] = 1
        fp_n.write(cooked)
    length = i + 1
    fp_p.write("__END__\n")
    fp_p.close()
    fp_n.write("__END__\n")
    fp_n.close()
    return len(detect_p.keys()), length


def kenshutsu_2(l_):
    return False


class MaxScore(object):
    """Retains and can update score values of detection evaluation."""
    def __init__(self):
        self.prec = {"nm": "PREC"}
        self.recl = {"nm": "RECL"}
        self.fval = {"nm": "FVAL"}
        self.accu = {"nm": "ACCU"}
        self.fam1 = {"nm": "FAM1"}
        self.fam2 = {"nm": "FAM2"}

    def update(self, thr, pr, rc, fv, ac, fam1, fam2) -> None:
        self.prec[thr] = pr
        self.recl[thr] = rc
        self.fval[thr] = fv
        self.accu[thr] = ac
        self.fam1[thr] = fam1
        self.fam2[thr] = fam2

    def show_max(self) -> None:
        for dic in [self.prec, self.recl, self.fval, self.accu, self.fam1, self.fam2]:
            dic2 = dict([(k, v) for k, v in dic.iteritems() if k != "nm"])
            mv = max(dic2.values())
            dic_inv = dict([(v, k) for k, v in dic2.iteritems()])
            size = dic_inv[mv]
            print("MAX:", dic["nm"], size, mv)


#
#
# MAIN
# 分類テストを行う関数
#
#


def clas_threM(
        thre_min: str,
        thre_max: str,
        tgt_dir: str,
        sizes: List[str]) -> None:
    """子音音韻類似度の閾値のグリッドサーチを補助する関数．
       事例数を起動時引数から受け取り、
       子音類似度閾値を変化させて精度・再現率・F値・正解率の最大値を出力"""

    ms = MaxScore()

    ft_type = 3  # 素性グループをここで指定 (タイプは 168 行目)
    no_euphonic = False  # 音便形[っんー]を削除する?
    print("ft_type: %d / no_euphonic: %d" % (ft_type, 1 if no_euphonic else 0))

    for size in sizes:
        tgt_p, tgt_n = "%s/positive_%s.txt" % (tgt_dir, size),\
                       "%s/negative_%s.txt" % (tgt_dir, size)
        print(tgt_p, tgt_n, thre_min, thre_max)
        print("Threshld:  Prec, Recl, F1,   Accu, TruN, a(F1+Ac), a(F1*Ac)")
        thre_min_i, thre_max_i = int(thre_min), int(thre_max)
        for i in range(thre_min_i - 1, thre_max_i):
            val = 0.01 * (i + 1)
            #ks = test_detect_from_file(val, size, tgt_p, tgt_n,
            #                           ft_type=ft_type, euphonic=no_euphonic)
            ks = None  # FIXME
            if not ks:
                print("Couldn't measre performance with value {}".format(val))
                continue
            prec, recl, fval, accu, fam1, fam2 = ks
            ms.update(val, prec, recl, fval, accu, fam1, fam2)  # USING GLOBALS
        if thre_max_i - thre_min_i > 0:
            ms.show_max()
    kk.jus.juman_close()


if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage:\n    oyomi.py <thre_min :int> <thre_max :int> <target\
 directory> <list of sizes (separated by spaces)>", file=sys.stderr)
    else:
        thre_min, thre_max, tgt_dir = sys.argv[1], sys.argv[2], sys.argv[3]
        sizes = sys.argv[4:]
        print("=" * 20, "START", datetime.now().ctime(), "=" * 20)
        # PとNのファイルを受取り、ルールの検出性能を評価
        clas_threM(thre_min, thre_max, tgt_dir, sizes)
