#coding: utf-8
"""Converts hiragana-retsu into sequence of mora."""

from __future__ import print_function
import re
from phon_engine.mora import DelimiterMora, Mora, NonPhonemicMora, MoraNIL
from phon_engine.tables import (P_CONSONANT, P_EUPHONIC, P_VOWEL,
                                phon_tbl0, phon_tbl1, phon_tbl2, phon_tbl3)

from typing import List

re_spaces = re.compile(r"\s+")
re_mora_split = re.compile(r'^(.*?)([{0}]{{0,2}}?[{1}]|[{2}])(.*?)$'.format(
    u''.join(P_CONSONANT), u''.join(P_VOWEL), u''.join(P_EUPHONIC)))


def moratize(ms_str: str) -> List[Mora]:
    """Convert a single string into list of Mora objects.

    params:
        (unicode) string: text of space-separated sequence of morae, with
            '||' representing morpheme boundary.
            e.g. 'fu to N || ga || fu Q to || N || da'
    returns:
        list of Mora objects, including Mora, DelimiterMora and NonPhonemicMora

    >>> moratize("fu to N || ga || fu Q to || N || da")
    [/fu/, /to/, /N/, ||, /ga/, ||, /fu/, /Q/, /to/, ||, /N/, ||, /da/]
    """
    ret = []
    for tbl in [phon_tbl0, phon_tbl1, phon_tbl2, phon_tbl3]:
        for ph in tbl:
            ms_str = ms_str.replace(ph, tbl[ph] + u" ")
    ms_str = (re_spaces.sub(u" ", ms_str)).strip()
    ret = [moratize_element(m_u) for m_u in ms_str.split(u" ") if m_u]
    return ret


def moratize_element(mora_u) -> Mora:
    mora_part_match = re_mora_split.match(mora_u)
    if mora_u == u"||":
        return DelimiterMora(mora_u)
    elif mora_part_match:
        nonmora_part_before = mora_part_match.group(1)
        mora_part = mora_part_match.group(2)
        nonmora_part_after = mora_part_match.group(3)

        if nonmora_part_before:
            return NonPhonemicMora(nonmora_part_before)
        if mora_part:
            return Mora(cons=mora_part[:-1], vow=mora_part[-1])
        if nonmora_part_after:
            return NonPhonemicMora(nonmora_part_after)
    return MoraNIL


def d_i(dic: dict) -> dict:
    return dict([(v, k) for k, v in dic.items()])


def mora_inverse(line):
    tables_inverse = [d_i(t) for t in [phon_tbl0, phon_tbl1, phon_tbl2]]
    for tbl_i in tables_inverse:
        for ph in tbl_i.keys():
            line = line.replace(ph, tbl_i[ph] + u" ")
    return line[:-1]


if __name__ == "__main__":
    ipt = u"しゃーんとぅあ"
    print(moratize(ipt))
