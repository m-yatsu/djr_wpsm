# coding: utf-8
"""Functions to process chou-on's (prolonged vowels)."""
from phon_engine.mora import Mora, MoraNIL
from typing import List


def chouon_obj(moras: List[Mora]) -> List[Mora]:
    """If sequential moras have chouon('ー'), convert it into a form of
    long vowel (same vowels across 2 moras).

    ### Examples
    /to/ /-/ /to/ /ro/ /Zi/ /-/ (とーとろじー) => /to/ /o/ /to/ /ro/ /Zi/ /i/
    /ku/ /tsu/ || /o/ || /ha/ /ko/ /u/ (くつ||を||はこう) => /ku/ /tsu/ || /o/ || /ha/ /ko/ /o/
    /ko/ || /o/ || /u/ /mu/ (こ||を||うむ) => /ko/ || /o/ || /u/ /mu/  # Unchanged

    args:
        moras:  list of Mora objects
    returns:
        ret:    list of Mora objects
    """
    ret = []
    moraprev = MoraNIL
    moraprevprev = MoraNIL

    for mora in moras:
        moraprev_has_cons = type(moraprev) is Mora and not moraprev.is_vowel()
        mora_has_contvow = any((
            mora.surface == "-" and type(moraprev) is Mora and moraprev.vow,
            moraprev_has_cons and (moraprev.vow, mora.surface) in {(u"o", u"u"), (u"e", u"i")}
        ))
        f_updated = False
        # Within delimiters: accept changes U->O, I->E and DASH->V
        # ex. '/ka/ /-/ || /so/ /u/ || /te/ /i/' ==> '/ka/ /a/ || /so/ /o/ || /te/ /e/'
        if all((type(mora) is Mora,
                type(moraprev) is Mora,
                not moraprev.is_delimiter,
                mora_has_contvow)):
            mora.update(cons='', vow=moraprev.vow)
            f_updated = True
        # Across delimiters: accept only changes DASH->V
        # ex. '/pa/ || /-/, /go/ || /-/' ==> '/pa/ || /a/ /go/ || /o/'
        elif all((moraprevprev,
                  type(moraprevprev) is Mora,
                  moraprev.is_delimiter,
                  mora.surface == u"-")):
            mora.update(cons='', vow=moraprevprev.vow)
            f_updated = True

        if f_updated:
            assert mora.is_vowel(), f"Chouon-Mora conversion \
                failure: sequence was '{moraprevprev}' '{moraprev}' '{mora}'"

        moraprevprev = moraprev
        moraprev = mora
        ret.append(mora)

    return ret
