#coding: utf-8
"""Morpheme analysis functions for dajare.

This library requires:
 - MeCab (morphological analyser) built for UTF-8 encoding
 - IPA dictionary for MeCab (not expanded one) built for UTF-8 encoding
 - Correctly configured mecabrc (placed in djr_wpsm/phon_engine/djr.mecabrc)
 + JUMAN (morphological analyser) 7.x
 + Kakasi (morphological analyser)

"""
from __future__ import print_function

import os
import re
import sys

import configparser
#import subprocess as commands

import MeCab
import phon_engine.my_juman as mj
import sudachipy.dictionary
import sudachipy.tokenizer

from typing import Tuple, List
from icecream import ic

ENC = "utf-8"
FPATH_INI = "config.ini"
TXT_TEST = "モナカ食べようと思ったら、もう無かった"
cp = configparser.SafeConfigParser()
if os.path.exists(FPATH_INI):
    cp.read(FPATH_INI)
else:
    sys.stderr.write("Config file was not found: " + FPATH_INI)
    sys.exit(2)

# MeCab
mecabrc_path = cp['Tools']['mecabrc_path']
mecabdicdir_path = cp['Tools']['mecabdicdir_path']
ic(mecabrc_path)
CYomi = MeCab.Tagger("-r {} -d {} -Oyomi".format(mecabrc_path, mecabdicdir_path))
CWakati = MeCab.Tagger("-r {} -d {} -Owakati".format(mecabrc_path, mecabdicdir_path))
CWakYom = MeCab.Tagger("-r {} -d {} -Owakatiyomi".format(mecabrc_path, mecabdicdir_path))
CPoS = MeCab.Tagger("-r{} -d {} -Opos".format(mecabrc_path, mecabdicdir_path))

# SudachiPy
CSudachi = sudachipy.dictionary.Dictionary().create()


def yomi_sudachi(txt: str) -> str:
    split_mode = sudachipy.tokenizer.Tokenizer.SplitMode.C
    ignored_rf = ["キゴウ"]
    return "".join(t.reading_form() for t
                   in CSudachi.tokenize(txt, split_mode)
                   if t.reading_form() not in ignored_rf)

# JUMAN, Kakasi and NKF (WILL NEVER BE USED)
J = mj.MyJuman(cp['Tools']['juman_path'])
jumanserver_port = int(cp['Tools']['jumanserver_port'])
J.juman_socket("localhost", jumanserver_port)
kakasi_path = cp['Tools']['kakasi_path']
nkf_path = cp['Tools']['nkf_path']

# 正規表現 (前処理用)
re_chouon = re.compile(r"ー+|[〜〜~]+")
re_NONJPC = re.compile(r'[^一-龠ぁ-んァ-ヴ。．、，.,!?！？ーa-zA-Z0-9０-９]+')
re_NONMOR = re.compile(r'[^ |ァ-ヴ]+')
# 正規表現 (MeCab.node に対する句読点等除去処理用)
#re_NOMORPH = re.compile(r"[^ァ-ヴ ]+")
re_punct = re.compile(r"[・、，。．,.！？!?「」『』”]")
re_spc = re.compile(r" +")


def convert_pp(parsed, poslst):
    """係助詞である「は」の発音を「わ」に置換する.

    params:
        (unicode) parsed: space-separated moras with morpheme delimiters.
        (list of unicode) poslst: POS tags
    returns:
        (unicode) ret: processed result
    """
    #ic(parsed)
    parsedA = parsed.split(u" || ")
    ret = []
    for morph, pos in zip(parsedA, poslst):
        if (morph, pos) == (u"ハ", u"助詞係助詞"):
            morph = u"ワ"
        ret.append(morph)
    return u" || ".join(ret)


def owakati_mecab_line(text: str = TXT_TEST):
    """MeCab で入力 (unicode型) に対する分かち区切文字列 (unicode型) を返す"""
    return CWakati.parse(text.encode(ENC)).decode(ENC).strip().replace(u" ", u" || ")


def oyomi_wakati_juman_line(text: str = TXT_TEST):
    """JUMANで読み＋分かち 配列を返す"""

    jr_gen = J.post_juman(text)
    jrA = (e for e in jr_gen if e != "EOS\n" and not e.startswith("@"))
    jyA = [e.split(" ")[1] for e in jrA]
    return " || ".join(jyA)


def oyomi_mecab_line(text=TXT_TEST):
    return CYomi.parse(text)


def get_yomi_hinshi(text_: str = TXT_TEST):
    """
    Params:
        (unicode) text_
    Returns:
        (unicode) ret
        (unicode) posA
    """
    ret, posA = oyomi_wakati_mecab_line(text_)
    #print "owmkl_1:", ret, "type:", type(ret)#d
    #if re_NONMOR.search(ret):
    #    ret = oyomi_kakasi_line(ret)#
    #print "owmkl_2:", ret, "type:", type(ret)#d
    return ret, posA


def oyomi_wakati_mecab_line(
        text: str = TXT_TEST,
        nosep: bool = False) -> Tuple[str, List[str]]:
    """MeCabで分かち書き(カタカナ)および品詞列取得を行う．
    入力:
        (unicode) text
    返り値:
        (unicode) ret: 分かち書きカタカナ読み
        (unicode) posA: 品詞列
    解析例:
        Input :なか食べようと思ったら、もう、無かった。
        Output:("モ || ナカ || タベヨ || ウ || ト || オモッ || タラ || モウ || ナカッ || タ",
    ["助詞係助詞","名詞一般","動詞自立","助動詞","助詞格助詞引用","動詞自立","助動詞","記号読点","副詞一般","記号読点","形容詞自立","助動詞","記号句点"])
    """

    ret = u""
    text = re_chouon.sub(u'ー', text)
    text = re_NONJPC.sub(u"", text)
    CWakYom.parse("")  # 一度空文字列のparseが必要
    psd = CWakYom.parse(text).strip()
    ret = re_spc.sub(u"" if nosep else u" || ", psd)
    posA = CPoS.parse(text).strip().split()
    #ic(ret)
    #ic(posA)
    return ret, posA


if __name__ == "__main__":
    sample = u"こんにちは世界さん，今日はいい日ですね"
    rslt_str, rslt_posA = oyomi_wakati_mecab_line(sample)
    print(rslt_str.encode(ENC))
    print(u" ".join(rslt_posA).encode(ENC))
