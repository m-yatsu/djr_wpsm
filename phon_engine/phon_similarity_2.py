#!/usr/bin/env python
# coding: utf-8
"""配列内音素列の一致部分の検出"""

from __future__ import print_function
import re
from math import log


import my_config
#import phon_engine.convert_mora as cm
from phon_engine.tables import MoraTable


MoraTable.Table012P = {}
for m in MoraTable.Table012:
    if m in (u'N', u'Q', u'-'):
        MoraTable.Table012P[m] = 5
    elif m in (u''):
        MoraTable.Table012P[m] = 3
    else:
        MoraTable.Table012P[m] = 1

FPATH_INI = "config.ini"

# 子音O/E比 (実験中のコーパスデータに基づいて算出 ver 1)
with my_config.MyConfigParser(FPATH_INI) as cp:
    FPATH_DIC_O = cp['Values']['fpath_dic_obs']
    FPATH_DIC_E = cp['Values']['fpath_dic_exp']
    OE_observed_corpus = None
    OE_expected_corpus = None

# TODO: 子音O/E比 (Kawahara らによる値)

#
#
# SENTENCE MODIFICATION
#
#


def supress_euphonic(
        text: str,
        _table: dict = str.maketrans({"ん": None, "っ": None, "ー": None, "、": None, ",": None, "　": None, " ": None})):
    """促撥長音を事前削除"""
    #re_chars_removed = re.compile(r"[んっー、,　\s]")
    return text.translate(_table)


#
#
# AGREEMENT CHECK
#
#

def agreemorphnum(morphsA=[]):
    dic = {}
    for mor in morphsA:
        if mor in dic:
            dic[mor] += 1
        else:
            dic[mor] = 1
    sums = dic.values()
    ret = sum([x - 1 for x in sums])
    return ret


def agree0(smallA, largeA):
    """小さな配列smallAが大きな配列largeAに
    包含される場合にその範囲を返す"""
    lgt0 = len(smallA)
    lgt1 = len(largeA)
    # もし音素列長さが1であれば処理せず
    if lgt0 <= 1:
        return []
    ret = []
    for i in range(0, lgt1 - lgt0 + 1):
        j, k = i, i + lgt0
        a = largeA[j:k]
        if smallA == a:
            ret.append((j, k))
    return ret


def chou(vowA, idx):
    return vowA[idx - 1]


def agree_threshold_ins(smallAT, largeAT, st, ed, thres,
                        use_vow_edist=True, use_cns_edist=True, use_cns_obexr=True, ls_r_tmp=None):
    """一般化を施した状態の列に対して
    agree_exact() を適用し，LS距離の逆数が閾値以上なら検出結果を返す

    添え字, (子音部分, 母音部分) の処理用データに加工する．
    同添え字の母音部分が同じであるかおよび子音部分の類似度を比較するため．

    plus:

    母音部に下記の音素を挿入した列との一致を試みる。
    [u'N', u'Q']

    """
    ret = []
    ls_r = 1.0

    smallAc = [x[0] for x in smallAT]
    smallAv = [x[1] for x in smallAT]
    largeAc = [x[0] for x in largeAT]
    largeAv = [x[1] for x in largeAT]

    if ls_r_tmp:
        ls_r = ls_r_tmp
    else:
        if use_vow_edist: # 1/母音の編集距離
            ls_r *= 1.0 / (levenshtein(smallAv, largeAv) + 1.0)
            # make sure ls_r is in (0, 1.0]
        if use_cns_edist: # 1/子音の編集距離
            ls_r *= 1.0 / (levenshtein(smallAc, largeAc) + 1.0)
        if use_cns_obexr: # 子音のO/E比
            ls_r *= oeratio(smallAc, largeAc)
    ran = range(0, len(smallAv) - 1)

    # 促音・撥音挿入
    mora_ins = [u'N', u'Q']
    for ins in mora_ins:
        cand1 = [smallAv] + [smallAv[:i] + [ins] + smallAv[i:] for i in ran]
        for smallAv_i in cand1:
            m = agree_tuple(smallAv_i, largeAv, st, ed) if ls_r >= thres else []
            #if m: print(smallAv, smallAv_i, m) #d
            if m:
                ret.append(m)

    # 長音挿入
    cand2 = [smallAv] + [smallAv[:i] + [chou(smallAv, i)] + smallAv[i:] for i in ran]
    for smallAv_i in cand2:
        m = agree_tuple(smallAv_i, largeAv, st, ed) if ls_r >= thres else []
        if m:
            ret.append(m)

    # 両方
    for ins in mora_ins:
        cand1 = [smallAv] + [smallAv[:i] + [chou(smallAv, i)] + [ins] + smallAv[i:] for i in ran]
        for smallAv_i in cand1:
            m = agree_tuple(smallAv_i, largeAv, st, ed) if ls_r >= thres else []
            if m:
                ret.append(m)

    return ret


def agree_NOthreshold(smallAT, largeAT, st, ed):
    """
    agree_exact() を適用し，LS距離の逆数が閾値以上なら検出結果を返す
    """

    return agree_tuple(smallAT, largeAT, st, ed)


def agree_threshold2_withScore(smallAT, largeAT, st, ed, thres,
                               use_vow_edist=True, use_cns_obexr=True, ls_r_tmp=None):
    """返り値が([一致部分リスト], スコア)となる以外，
    下のagree_threshold2()と同じ"""
    ls_r = 1.0

    smallAc = [x[0] for x in smallAT]
    smallAv = [x[1] for x in smallAT]
    #print("largeAT:", largeAT) #d
    largeAc = [x[0] for x in largeAT]
    largeAv = [x[1] for x in largeAT]
    if ls_r_tmp:
        ls_r = ls_r_tmp
    else:
        if use_vow_edist: # 1/母音の編集距離
            ls_r *= 1.0 / (levenshtein(smallAv, largeAv) + 1.0) # make sure ls_r is in (0, 1.0]
        if use_cns_obexr: # 子音のO/E比
            ls_r *= oeratio(smallAc, largeAc)
    return (agree_tuple(smallAv, largeAv, st, ed), ls_r) if ls_r >= thres else ([], 0.0)


def agree_threshold2(smallAT, largeAT, st, ed, thres,
                     use_vow_edist=True, use_cns_obexr=True, ls_r_tmp=None):
    """一般化を施した状態の列に対して
    agree_exact() を適用し，LS距離の逆数が閾値以上なら検出結果を返す

    添え字, (子音部分, 母音部分) の処理用データに加工する．

    use_vow_edist:
        同添え字の母音部分が同じであるか
    use_cns_obexr:
        音部分の類似度を比較する

    ls_r_tmp:
        スコアの一時保存
    """
    ls_r = 1.0

    smallAc = [x[0] for x in smallAT]
    smallAv = [x[1] for x in smallAT]
    #print("largeAT:", largeAT) #d
    largeAc = [x[0] for x in largeAT]
    largeAv = [x[1] for x in largeAT]
    if ls_r_tmp:
        ls_r = ls_r_tmp
    else:
        if use_vow_edist: # 1/母音の編集距離
            ls_r *= 1.0 / (levenshtein(smallAv, largeAv) + 1.0)  # make sure ls_r is in (0, 1.0]
        if use_cns_obexr: # 子音のO/E比
            ls_r *= oeratio(smallAc, largeAc)
    return agree_tuple(smallAv, largeAv, st, ed) if ls_r >= thres else []


def agree_threshold(smallA, largeA, st, ed, thres, ls_r_tmp=None, funcname="lev"):
    """一般化を施した状態の列に対して
    agree_exact() を適用し，LS距離の逆数が閾値以上なら検出結果を返す

    添え字, (子音部分, 母音部分) の処理用データに加工する．
    同添え字の母音部分が同じであるかおよび子音部分の類似度を比較するため．

    ls_r_tmp:
        スコアの一時保存
    """

    if funcname == "lev":
        func = levenshtein
    elif funcname == "oer":
        func = oeratio

    if ls_r_tmp:
        ls_r = ls_r_tmp
    else:
        ls_r = 1.0 / (func(smallA, largeA) + 1.0)  # make sure ls_r is in (0, 1.0]
    #print("score", ls_r)
    return agree_exact(smallA, largeA, st, ed) if ls_r >= thres else []


def agree_tuple(smallA, largeA, st, ed):
    """個々をタプル化した音素列の一致検出．

    小さな配列smallA(種表現候補)は大きな配列largeAに含まれるものとする．
    largeAより範囲 st,ed の部分を切り捨て，左右の2配列(left, right)を得る．
    それぞれの配列xにagree0(x, largeA)を適用し，解(変形表現候補)をまとめる．

    largeAにおけるsmallAの真の区間と，(st, ed)の指し示すものが異なっている
    場合がある．

    入力:
        smallA(list): 小配列
        largeA(list): 大配列
        st,
        ed(int): 範囲
        dic_oeratio: 未使用"""

    largeA_left = largeA[:st]
    #print("agree_exact_left in", st, ed, largeA_left)
    # 当該部分の前の部分
    ret_left = agree0(smallA, largeA_left)
    largeA_right = largeA[ed:]
    # 当該部分の後の部分
    ret_right_after = []
    ret_right = agree0(smallA, largeA_right)
    for st_right, ed_right in ret_right:
        ret_right_after.append((st_right + ed, ed_right + ed))

    return ret_left + ret_right_after


def agree_exact(smallA, largeA, st, ed):
    """音素列の一致検出．

    小さな配列smallA(種表現候補)は大きな配列largeAに含まれるものとする．
    largeAより範囲 st,ed の部分を切り捨て，左右の2配列(left, right)を得る．
    それぞれの配列xにagree0(x, largeA)を適用し，解(変形表現候補)をまとめる．

    largeAにおけるsmallAの真の区間と，(st, ed)の指し示すものが異なっている
    場合がある．

    入力:
        smallA(list): 小配列
        largeA(list): 大配列
        st,
        ed(int): 範囲
        dic_oeratio: 未使用"""

    largeA_left = largeA[:st]
    #print("agree_exact_left in", st, ed, largeA_left)
    # 当該部分の前の部分
    ret_left = agree0(smallA, largeA_left)
    largeA_right = largeA[ed:]
    # 当該部分の後の部分
    ret_right_after = []
    ret_right = agree0(smallA, largeA_right)
    for st_right, ed_right in ret_right:
        ret_right_after.append((st_right + ed, ed_right + ed))

    return ret_left + ret_right_after


def zero(num):
    s = 0.0001
    return s if num < s else num

#
#
# PHONETIC SIMILARITY
#
#


def consonant_similarity(c1, c2, score_same):
    """Calculate consonant similarity."""
    #TODO:TODO:TODO:TODO:TODO:TODO
    # FIXME: 子音がO/E辞書のキーが存在しないために負値になる点．
    # 妥当な値に変更する．
    # FIXME: O/E辞書を再構築する．
    #TODO:TODO:TODO:TODO:TODO:TODO
    if c1 == c2:
        return score_same

    #sim = OE_expected_corpus[c1][c2] - OE_observed_corpus[c1][c2]
    bunshi, bunbo = 0.0, 1.0
    if c1 in OE_observed_corpus:
        bunshi = OE_observed_corpus.get(c1).get(c2)
    else:
        bunshi = 0.0
    if c1 in OE_expected_corpus:
        bunbo = OE_expected_corpus.get(c1).get(c2)
    else:
        bunbo = 1.0
    if not bunshi:
        bunshi = 0.0
    if not bunbo:
        bunbo = 1.0

    #sim = OE_observed_corpus.get(c1).get(c2) / 1.0 / zero(OE_expected_corpus[c1][c2])
    if bunshi > 0.0:
        sim = log(bunshi / bunbo)
    else:
        sim = 0.0
    #if sim > 1000:#d
    #    print("(%s, %s)" % (c1, c2, ), bunshi, "/", bunbo)#d
    return sim


def oeratio(s1, s2):
    #print(s1, s2) #d
    if len(s1) < len(s2):
        return oeratio(s2, s1)
    if len(s2) == 0:
        return -1
    score = 0.0
    for i, c2 in enumerate(s2):
        c1 = s1[i].encode('utf-8')
        c2 = c2.encode('utf-8')
        sim = consonant_similarity(c1, c2, 0.5)
        score += sim
    return score


def levenshtein(s1, s2):
    """From http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Python"""
    if len(s1) < len(s2):
        return levenshtein(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    row_prev = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        row_curr = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = row_prev[j + 1] + 1
            # j + 1 instead of j, since previous_row and
            # current_row are both one character longer
            # than s2
            deletions = row_curr[j] + 1
            substitutions = row_prev[j] + (c1 != c2)
            row_curr.append(min(insertions, deletions, substitutions))
        row_prev = row_curr

    return row_prev[-1]

#
#
# MORA-BASED SIMILARITY
#
#


def mora_compare_similarity(expA, expB, thres=1.0):
    """TODO: 正規化する"""
    print("cs input:", expA, expB)
    if not (expA and expB):
        return 0.0
    cmpr_rslt = mora_compare(expA, expB)
    #score_same = thres / 1.0 / len(expA)
    score_same = thres
    ret_score = 0.0
    for vwl, pair in cmpr_rslt:
        c1, c2 = pair
        ret_score += consonant_similarity(c1, c2, score_same)
    return ret_score


def mora_compare(expA, expB):
    """モーラ列(list) expA, expB
    から，母音が一致するが子音が不一致のペア: ただし、
    位置が重ならないものを全て抽出する．
    """
    w_vowA = []
    ret_csnt_pairs = []
    for e in expA:
        if re.match(r'.*[aieou]$', e):
            w_vowA.append(e)
    i = -1
    for e in w_vowA:
        i += 1
        if i > len(expB) - 1:
            break
        for e2 in expB[i:]:
            if not e2:
                continue
            v = e[-1]
            v2 = e2[-1]
            c = e[:-1]
            c2 = e2[:-1]
            if v == v2: # 母音が同じであり，かつ
                if c == c2: # 子音が異なる場合のみを対象
                    continue
                ret_csnt_pairs.append((e[-1], (c, c2)))
                break
    return ret_csnt_pairs

#
#
# NORMALIZATION
#
#


def ippanka(ary):
    """
    ＊旧ver
    モーラ列(unicodestringlist)一般化を行う (convert_mora.py使用)
    change_consonant() により呼び出し"""
    aryg = []
    for mor in ary:
        if mor in MoraTable.Table0:
            r = mor
        elif mor in MoraTable.Table1:
            r = mor[-1]
        elif mor in MoraTable.Table2:
            r = mor[-1]
        else:
            r = mor
        aryg.append(r)
    return aryg


def ippankaWithRest(ary):
    """
    ＊旧ver
    モーラ列(unicodestringlist)の一般化を行う (convert_mora.py上のテーブル使用)．
    change_consonant() により呼び出し
    子音の情報も返す．文字列 rest に子音部分が連結される

    e.g.
    In [x]: ippankaWithRest([u'mo', u'Ti'])
    Out[x]: ([u'o', u'i'], u'mT')
    """
    aryg = []
    rest = u""
    for mor in ary:
        rr = u""
        if mor in MoraTable.Table0:
            r = mor
        elif mor in MoraTable.Table1:
            r = mor[-1]
            rr = mor[:-1]
        elif mor in MoraTable.Table2:
            r = mor[-1]
            rr = mor[:-1]
        else:
            r = mor
        aryg.append(r)
        rest += rr
    return aryg, rest


def ippankaWithRest2(moraU):
    """モーラ列(unicodestring)を形態素境界('||')を越えない範囲で
    一般化する(母音のみにする)

    Params:
        moraU -- 形態素境界 || で区切られた，モーラ音素列 (unicode)
    Returns:
        retAA -- 形態素に対応するモーラ列を要素とするリスト"""

    morphsAA = [s.strip().split(u" ") for s in moraU.split(u"||")]
    retAA = []
    for morphA in morphsAA:
        tmpA = []
        for mor in morphA:
            cons = u""
            if mor in MoraTable.Table0:
                vow = mor
            elif mor in MoraTable.Table1:
                vow = mor[-1]
                cons = mor[:-1]
            elif mor in MoraTable.Table2:
                vow = mor[-1]
                cons = mor[:-1]
            else:
                vow = mor
            tmpA.append((cons, vow))
        retAA.append(tmpA)
    return retAA


def change_consonant(ary0, ary1, st, ed):
    """ary0, ary1内の各モーラの子音を一般化する．
    その状態で agree_exact() を実行して，一致したら万歳!"""
    ary0g = ippanka(ary0)
    ary1g = ippanka(ary1)
    return agree_exact(ary0g, ary1g, st, ed)

#
#
# MATCHFUNC'S
#
#
# 下記の関数は，パラメタvalを受けて関数を返す．


def ccWL_val(val):
    thres = val

    def change_consonant_withlimit(ary0, ary1, st, ed):
        """ary0, ary1内の各モーラの子音を一般化する．
        その状態で agree_exact() を実行して，一致したら万歳!
        ただし，LS距離が閾値 thres を上回っていたらダメなver．"""
        ary0g, ary0R = ippankaWithRest(ary0)
        ary1g, ary1R = ippankaWithRest(ary1)
        #thres = theshold #### 子音部分を集めたもの同士のLS距離の上限 ####
        return agree_threshold(ary0g, ary1g, st, ed, thres)
    return change_consonant_withlimit


def ccWL_valR(val):
    """ary0, ary1内の各モーラの子音を一般化する．
    そのまま agree_exact() を実行して，一致したら万歳!
    ただし，LS距離が閾値 thres を上回っていたらダメなver．
    ただし，相対値に正規化．
    ただし，閾値を外から指定できるver."""
    thres = val

    def change_consonant_withlimit(ary0, ary1, st, ed):
        ary0g, ary0R = ippankaWithRest(ary0)
        ary1g, ary1R = ippankaWithRest(ary1)
        #thres = theshold #### 子音部分を集めたもの同士のLS距離の上限 ####
        #ls_r = 1.0 / (levenshtein(ary0R, ary1R) + 1.0) # 0 < ls_r <= 1.0

        return agree_threshold(ary0g, ary1g, st, ed, thres)
    return change_consonant_withlimit


def insertion_ccWL_valR(val):
    """全ての可能なモーラに対して，(テストで1種類だけ)
    ＋そして種形態素内の全ての挿入可能位置にたいして
    モーラの挿入をこころみ，種形態素と短文の音韻のマッチを行う．
    ただし，LS距離が閾値 thres を上回っていたらダメなver．
    ただし，相対値に正規化．
    ただし，閾値を外から指定できるver."""
    moras = [u"a"]
    thres = val

    def function0(ary0, ary1, st, ed):
        queue = []
        locs_ins = range(1, len(ary0)) # [1, ..., len(ary0)-1]
        for mor in moras:
            #挿入対象モーラけってい
            for loc in locs_ins:
                ary0i = [x for x in ary0]
                #挿入対象種表現の作成
                ary0i.insert(loc, mor)
                #挿入対象種表現と短文の音韻類似性比較
                ary0g, ary0R = ippankaWithRest(ary0i)
                ary1g, ary1R = ippankaWithRest(ary1)
                #結果を取得, キューへの追加
                queue += agree_threshold(ary0g, ary1g, st, ed, thres)
        return queue #うまくいったモーラと種のペアを得る
    return function0


def insertion_ccWL_valR_full(val):
    """全ての可能なモーラに対して，(本当に全て)
    ＋そして種形態素内の全ての挿入可能位置にたいして
    モーラの挿入をこころみ，種形態素と短文の音韻のマッチを行う．
    ただし，LS距離が閾値 thres を上回っていたらダメなver．
    ただし，相対値に正規化．
    ただし，閾値を外から指定できるver."""
    moras = MoraTable.Table012
    thres = val

    def function1(ary0, ary1, st, ed):
        queue = []
        locs_ins = range(1, len(ary0)) # [1, ..., len(ary0)-1]
        for mor in moras:
            #挿入対象モーラけってい
            for loc in locs_ins:
                ary0i = [x for x in ary0]
                #挿入対象種表現の作成
                ary0i.insert(loc, mor)
                #挿入対象種表現と短文の音韻類似性比較
                ary0g, ary0R = ippankaWithRest(ary0i)
                ary1g, ary1R = ippankaWithRest(ary1)
                #結果を取得, キューへの追加
                #ls_r = 1.0 / (levenshtein(ary0R, ary1R) + 1.0) # 0 < ls_r <= 1.0
                queue += agree_threshold(ary0g, ary1g, st, ed, thres)
        #うまくいったモーラと種のペアを得る
        return queue
    return function1


def insertion_ccWL_valR_full_CSNTIM(val):
    """全ての可能なモーラに対して，(本当に全て)
    ＋そして種形態素内の全ての挿入可能位置にたいして
    モーラの挿入をこころみ，種形態素と短文の音韻のマッチを行う．
    ただし，LS距離が閾値 thres を上回っていたらダメなver．
    ただし，相対値に正規化
    ただし，閾値を外から指定できるver.
    ただし，子音類似度を加算するver."""
    #moras = MoraTable.Table012
    moras = MoraTable.Table12
    thres = val

    def function1(ary0, ary1, st, ed):
        """関数内関数"""
        queue = []
        locs_ins = range(1, len(ary0)) # [1, ..., len(ary0)-1]
        for mor in moras:
            #挿入対象モーラけってい
            for loc in locs_ins:
                ary0i = [x for x in ary0]
                #挿入対象種表現の作成
                ary0i.insert(loc, mor)
                #挿入対象種表現と短文の音韻類似性比較
                ary0g, ary0c = ippankaWithRest(ary0i)
                ary1g, ary1c = ippankaWithRest(ary1)
                #結果を取得, キューへの追加
                # 子音列のレーベンシュタイン距離
                ls_r_sim1 = 1.0 / (levenshtein(ary0c, ary1c) + 1.0)
                ls_r_sim2 = mora_compare_similarity(ary0c, ary1c, 1.0) + 0.001
                ls_r = ls_r_sim1 * ls_r_sim2
                # 0 < ls_r <= 1.0
                agreement = agree_threshold(ary0g, ary1g, st, ed, thres, ls_r)
                queue += agreement
        #うまくいったモーラと種のペアを得る
        return queue
    return function1


def test():
    # test 1
    t00A = [1, 4]
    t01A = [1, 5, 3, 0, 1, 4, 2, 6, 1, 4, 1]
    agr = agree0(t00A, t01A)
    print("Test 1: Agreement extraction <testing agree0()>")
    print("=" * 10)
    print(t00A, "vs", t01A)
    print("agreement:", agr)
    print()

    # test 2
    print("Test 2: Similarity calculation")
    print("=" * 10)
    exp0 = [u'sa', u'Si', u'mi']
    exp1 = [u'ta', u'ki', u'bi']
    exp2 = [u'wa', u"sa", u"bi"]
    print("Exp 0:", exp0)
    print("Exp 1:", exp1)
    print("Exp 2:", exp2)
    print("sim(exp0, exp1) =", mora_compare_similarity(exp0, exp1))
    print("sim(exp1, exp2) =", mora_compare_similarity(exp1, exp2))
    print("sim(exp0, exp2) =", mora_compare_similarity(exp0, exp2))
    print("sim(exp0, exp0) =", mora_compare_similarity(exp0, exp0))


if __name__ == "__main__":
    test()
