# coding: utf-8
"""Class definition of Japanese mora."""

from typing import Tuple


ENC = "utf-8"


class Mora(object):

    def __init__(self, cons=u"", vow=u"") -> None:
        self.update(cons, vow)
        self.is_delimiter: bool = False  # is a morpheme delimiter
        self.is_nonmora: bool = False    # is a non-phonemic mora

    def __unicode__(self) -> str:
        return self.surface

    def __str__(self) -> str:
        return f"/{self.surface}/"

    def __repr__(self) -> str:
        return self.__str__()

    def is_vowel(self) -> bool:
        return self.cons == u""

    def update(self, cons: str, vow: str) -> None:
        self.cons: str = cons
        self.vow: str = vow
        self.set_surface(self.cons + self.vow)

    def set_surface(self, s: str) -> None:
        self.surface: str = s

    def cv(self) -> Tuple[str, str]:
        return self.cons, self.vow


class NonPhonemicMora(Mora):
    """Used to refer to mora of non-phonemic characters.
    Deleted or replaced during language model generation."""

    def __init__(self, surface: str) -> None:
        self.cons, self.vow = '', ''
        self.is_delimiter = False
        self.is_nonmora = True
        self.surface = surface

    def __str__(self) -> str:
        return "'{self.surface}'"

    def __repr__(self) -> str:
        return self.__str__()


class DelimiterMora(NonPhonemicMora):
    """Morpheme delimiter.
    Treated as one kind of Mora for ease of use."""

    def __init__(self, surface=u'||'):
        self.cons, self.vow = '', ''
        self.is_delimiter = True
        self.is_nonmora = True
        self.surface = surface

    def __str__(self) -> str:
        return f"{self.surface}"

    def __repr__(self) -> str:
        return self.__str__()


class DummyMora(NonPhonemicMora):
    """Used for padding when two sequences of Moras are aligned."""

    def __init__(self):
        self.cons, self.vow = '', ''
        self.is_delimiter = False
        self.is_nonmora = True
        self.surface = ''

    def __str__(self) -> str:
        return "DUMMY"

    def __repr__(self) -> str:
        return self.__str__()


class NilMoraClass(NonPhonemicMora):
    """NULL object besides any instances of Mora"""

    def __init__(self):
        self.cons, self.vow = None, None
        self.is_delimiter = False
        self.is_nonmora = True
        self.surface = None

    def __str__(self) -> str:
        return "NIL"

    def __repr__(self) -> str:
        return self.__str__()

MoraNIL = NilMoraClass()
