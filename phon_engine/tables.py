#coding: utf-8
"""Conversion tables which do:
1. katakana -> hiragana                          => kht
2. Alphabets -> hiragana                         => aht
3. hiragana (mora) -> phonetic forms (mora)      => phon_tbl*
"""
# flake8: noqa: E241


P_CONSONANT = [u'ny', u'S', u'y', u'd', u'sw', u's', u'ty', u'k', u'z', u'g', u'r', u'h',
               u's', u'n', u'hy', u'by', u'w', u'f', u'py', u't', u'ry', u'kw', u'T', u'ky', u'dZ', u'my',
               u'gy', u'dz', u'kw', u'p', u'b', u'tw', u'Z', u'dy', u'm', u'gw', u'v']
P_VOWEL = [u'i', u'e', u'a', u'o', u'u']
P_EUPHONIC = [u'N', u'Q', '-']


kht = {u"ア": u"あ", u"イ": u"い", u"ウ": u"う", u"エ": u"え", u"オ": u"お",
       u"カ": u"か", u"キ": u"き", u"ク": u"く", u"ケ": u"け", u"コ": u"こ",
       u"ガ": u"が", u"ギ": u"ぎ", u"グ": u"ぐ", u"ゲ": u"げ", u"ゴ": u"ご",
       u"サ": u"さ", u"シ": u"し", u"ス": u"す", u"セ": u"せ", u"ソ": u"そ",
       u"ザ": u"ざ", u"ジ": u"じ", u"ズ": u"ず", u"ゼ": u"ぜ", u"ゾ": u"ぞ",
       u"タ": u"た", u"チ": u"ち", u"ツ": u"つ", u"テ": u"て", u"ト": u"と",
       u"ダ": u"だ", u"ヂ": u"ぢ", u"ヅ": u"づ", u"デ": u"で", u"ド": u"ど",
       u"ナ": u"な", u"ニ": u"に", u"ヌ": u"ぬ", u"ネ": u"ね", u"ノ": u"の",
       u"ハ": u"は", u"ヒ": u"ひ", u"フ": u"ふ", u"ヘ": u"へ", u"ホ": u"ほ",
       u"バ": u"ば", u"ビ": u"び", u"ブ": u"ぶ", u"ベ": u"べ", u"ボ": u"ぼ",
       u"パ": u"ぱ", u"ピ": u"ぴ", u"プ": u"ぷ", u"ペ": u"ぺ", u"ポ": u"ぽ",
       u"マ": u"ま", u"ミ": u"み", u"ム": u"む", u"メ": u"め", u"モ": u"も",
       u"ヤ": u"や", u"ユ": u"ゆ", u"ヨ": u"よ",
       u"ラ": u"ら", u"リ": u"り", u"ル": u"る", u"レ": u"れ", u"ロ": u"ろ",
       u"ワ": u"わ", u"ヰ": u"ゐ", u"ウ": u"う", u"ヱ": u"ゑ", u"ヲ": u"を",
       u"ン": u"ん",
       u"ァ": u"ぁ", u"ィ": u"ぃ", u"ゥ": u"ぅ", u"ェ": u"ぇ", u"ォ": u"ぉ",
       u"ャ": u"ゃ", u"ュ": u"ゅ", u"ョ": u"ょ",
       u"ヮ": u"ゎ", u"ッ": u"っ", u"ヴ": u"ゔ", }


alphabet_trans = [# A to E: 0 to 5 (2 variants for D)
                  u"えい",  u"びー",   u"しー",   u"でぃー",    u"でー",    u"いー",
                  # F to J: 6 to 11 (2 variants for H)
                  u"えふ",  u"じー",   u"えいち", u"えっち",    u"あい",    u"じぇい"
                  # K to O: 12 to 16
                  u"けい",  u"える",   u"えむ",   u"えぬ",      u"おー",
                  # P to T: 17 to 22 (2 variants for T)
                  u"ぴい",  u"きゅう", u"あーる", u"えす",      u"てぃー",  u"てー",
                  # U to X: 23 to 29 (2 variants for V, 2 for W, 2 for X)
                  u"ゆう",  u"ゔい",   u"ゔぃー", u"だぶりゅー", u"だぶる", u"えっくす", u"えくす",
                  # Y to Z: 30 to 32 (2 variants for Z)
                  u"わい",  u"ぜっと", u"ずぃー"]


aht = {u"A": (0,),     u"a": (0,),     u"Ａ": (0,),     u"ａ": (0,),
       u"B": (1,),     u"b": (1,),     u"Ｂ": (1,),     u"ｂ": (1,),
       u"C": (2,),     u"c": (2,),     u"Ｃ": (2,),     u"ｃ": (2,),
       u"D": (3, 4),   u"d": (3, 4),   u"Ｄ": (3, 4),   u"ｄ": (3, 4),
       u"E": (5,),     u"e": (5,),     u"Ｅ": (5,),     u"ｅ": (5,),
       u"F": (6,),     u"f": (6,),     u"Ｆ": (6,),     u"ｆ": (6,),
       u"G": (7,),     u"g": (7,),     u"Ｇ": (7,),     u"ｇ": (7,),
       u"H": (8, 9),   u"h": (8, 9),   u"Ｈ": (8, 9),   u"ｈ": (8, 9),
       u"I": (10,),    u"i": (10,),    u"Ｉ": (10,),    u"ｉ": (10,),
       u"J": (11,),    u"j": (11,),    u"Ｊ": (11,),    u"ｊ": (11,),
       u"K": (12,),    u"k": (12,),    u"Ｋ": (12,),    u"ｋ": (12,),
       u"L": (13,),    u"l": (13,),    u"Ｌ": (13,),    u"ｌ": (13,),
       u"M": (14,),    u"m": (14,),    u"Ｍ": (14,),    u"ｍ": (14,),
       u"N": (15,),    u"n": (15,),    u"Ｎ": (15,),    u"ｎ": (15,),
       u"O": (16,),    u"o": (16,),    u"Ｏ": (16,),    u"ｏ": (16,),
       u"P": (17,),    u"p": (17,),    u"Ｐ": (17,),    u"ｐ": (17,),
       u"Q": (18,),    u"q": (18,),    u"Ｑ": (18,),    u"ｑ": (18,),
       u"R": (19,),    u"r": (19,),    u"Ｒ": (19,),    u"ｒ": (19,),
       u"S": (20,),    u"s": (20,),    u"Ｓ": (20,),    u"ｓ": (20,),
       u"T": (21, 22), u"t": (21, 22), u"Ｔ": (21, 22), u"ｔ": (21, 22),
       u"U": (23,),    u"u": (23,),    u"Ｕ": (23,),    u"ｕ": (23,),
       u"V": (24, 25), u"v": (24, 25), u"Ｖ": (24, 25), u"ｖ": (24, 25),
       u"W": (26, 27), u"w": (26, 27), u"Ｗ": (26, 27), u"ｗ": (26, 27),
       u"X": (28, 29), u"x": (28, 29), u"Ｘ": (28, 29), u"ｘ": (28, 29),
       u"Y": (30,),    u"y": (30,),    u"Ｙ": (30,),    u"ｙ": (30,),
       u"Z": (31, 32), u"z": (31, 32), u"Ｚ": (31, 32), u"ｚ": (31, 32),
       }


phon_tbl0 = \
 {u"ん": u"N",
  u"っ": u"Q",
  u"ー": u"-",
  u"〜": u"-"}


phon_tbl1 = \
 {u"いぇ": u"ye",
  u"きゃ": u"kya", u"きぃ": u"ki", u"きゅ": u"kyu", u"きぇ": u"kye", u"きょ": u"kyo",
  u"くぁ": u"kwa", u"くぃ": u"kwi", u"くぅ": u"ku", u"くぇ": u"kwe", u"くぉ": u"kwo",
  u"ぐぁ": u"gwa", u"ぐぃ": u"gwi", u"ぐぅ": u"gu", u"ぐぇ": u"gwe", u"ぐぉ": u"gwo",
  u"くゎ": u"kwa",
  u"ぐゎ": u"gwa",
  u"すゎ": u"swa",
  u"とゎ": u"twa",
  u"しゃ": u"Sa", u"しぃ": u"Si", u"しゅ": u"Su", u"しぇ": u"Se", u"しょ": u"So",
  u"すぁ": u"sa", u"すぃ": u"si", u"すぅ": u"su", u"すぇ": u"swe", u"すぉ": u"swo",
  u"ちゃ": u"tSa", u"ちぃ": u"tSi", u"ちゅ": u"tSu", u"ちぇ": u"tSe", u"ちょ": u"tSo",
  u"てぃ": u"ti", u"とぅ": u"tu",
  u"つぁ": u"tsa", u"つぃ": u"tsi", u"つぇ": u"tse", u"つぉ": u"tso",
  u"てゃ": u"tya", u"てゅ": u"tyu", u"てょ": u"tyo",
  u"ひゃ": u"hya", u"ひゅ": u"hyu", u"ひぇ": u"hye", u"ひょ": u"hyo",
  u"ふぁ": u"fa", u"ふぃ": u"fi", u"ふぇ": u"fe", u"ふぉ": u"fo",
  u"ぎゃ": u"gya", u"ぎゅ": u"gyu", u"ぎぇ": u"gye", u"ぎょ": u"gyo",
  u"ぐぁ": u"gua", u"ぐぃ": u"gui", u"ぐぇ": u"gue", u"ぐぉ": u"guo",
  u"じゃ": u"Za", u"じゅ": u"Zu", u"じぇ": u"Ze", u"じょ": u"Zo",
  u"ぢゃ": u"dZa", u"ぢゅ": u"dZu", u"ぢぇ": u"dZe", u"ぢょ": u"dZo",
  u"でぃ": u"di", u"どぅ": u"du",
  u"づぁ": u"dza", u"づぃ": u"dzi", u"づぇ": u"dze", u"づぉ": u"dzo",
  u"でゃ": u"dya", u"でゅ": u"dyu", u"でょ": u"dyo",
  u"みゃ": u"mya", u"みゅ": u"myu", u"みぇ": u"mye", u"みょ": u"myo",
  u"にゃ": u"nya", u"にゅ": u"nyu", u"にぇ": u"nye", u"にょ": u"nyo",
  u"びゃ": u"bya", u"びゅ": u"byu", u"びぇ": u"bye", u"びょ": u"byo",
  u"ぴゃ": u"pya", u"ぴゅ": u"pyu", u"ぴぇ": u"pye", u"ぴょ": u"pyo",
  u"りゃ": u"rya", u"りゅ": u"ryu", u"りぇ": u"rye", u"りょ": u"ryo",
  u"ゔぁ": u"va", u"ゔぃ": u"vi", u"ゔゅ": u"vu", u"ゔぇ": u"ve", u"ゔぉ": u"vo", }


phon_tbl2 = \
{u"あ":  u"a", u"い":  u"i", u"う":  u"u", u"え":  u"e", u"お":  u"o",
 u"ぁ":  u"a", u"ぃ":  u"i", u"ぅ":  u"u", u"ぇ":  u"e", u"ぉ":  u"o",
 u"か":  u"ka", u"き":  u"ki", u"く":  u"ku", u"け":  u"ke", u"こ":  u"ko",
 u"が":  u"ga", u"ぎ":  u"gi", u"ぐ":  u"gu", u"げ":  u"ge", u"ご":  u"go",
 u"さ":  u"sa", u"し":  u"Si", u"す":  u"su", u"せ":  u"se", u"そ":  u"so",
 u"ざ":  u"za", u"じ":  u"Zi", u"ず":  u"zu", u"ぜ":  u"ze", u"ぞ":  u"zo",
 u"た":  u"ta", u"ち":  u"tSi", u"つ":  u"tsu", u"て":  u"te", u"と":  u"to",
 u"だ":  u"da", u"ぢ":  u"dZi", u"づ":  u"dzu", u"で":  u"de", u"ど":  u"do",
 u"な":  u"na", u"に":  u"ni", u"ぬ":  u"nu", u"ね":  u"ne", u"の":  u"no",
 u"は":  u"ha", u"ひ":  u"hi", u"ふ":  u"fu", u"へ":  u"he", u"ほ":  u"ho",
 u"ば":  u"ba", u"び":  u"bi", u"ぶ":  u"bu", u"べ":  u"be", u"ぼ":  u"bo",
 u"ぱ":  u"pa", u"ぴ":  u"pi", u"ぷ":  u"pu", u"ぺ":  u"pe", u"ぽ":  u"po",
 u"ま":  u"ma", u"み":  u"mi", u"む":  u"mu", u"め":  u"me", u"も":  u"mo",
 u"ら":  u"ra", u"り":  u"ri", u"る":  u"ru", u"れ":  u"re", u"ろ":  u"ro",
 u"わ":  u"wa", u"ゐ":  u"i", u"ゑ":  u"e", u"を":  u"o",
 u"ゔ":  u"vu",
 u"や":  u"ya", u"ゆ":  u"yu", u"よ": u"yo", }


phon_tbl3 = \
 {u"ゃ": u"ya", u"ゅ": u"yu", u"ょ": u"yo", u"ゎ": u"wa"}


class MoraTable:
  Table0 = set(phon_tbl0.values())
  Table1 = set(phon_tbl1.values())
  Table2 = set(phon_tbl2.values())
  Table012 = Table0 | Table1 | Table2 | set(u"-")
  Table12 = Table1 | Table2
