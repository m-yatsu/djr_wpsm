#coding: utf-8
"""Common functions/classes used for Tane-Henkei detection."""

import codecs
import re
import sys
import phon_engine.chouon_process as cp
import phon_engine.convert_mora as cm
import phon_engine.phon_similarity_2 as ps
import pickle
import subprocess as commands

#from typing import Callable

enc = "utf-8"

#
# Below code is based on
# http://d.hatena.ne.jp/mohayonao/20101213/1292237816
#


class Tr:
    """文字レベルの前処理"""
    re_katakana = re.compile(r'[ァ-ヴ]')
    re_hiragana = re.compile(r'[ぁ-ゔ]')
    re_non_hiragana = re.compile(r' ?[^ぁ-ゔ ] ?')
    re_kakko = re.compile(r'[(（][^()（）]+[)）『』「」]')
    re_pre2 = re.compile(r'[．。，、！？・＋＿；：＊・”’＝/／☆！？!?]')

    tbl_zenhan_number = {ord(n_wide): str(n_narrow)
                         for n_wide, n_narrow
                         in zip('０１２３４５６７８９', range(0, 10))}

    @classmethod
    def hiragana(cls, text: str) -> str:
        """ひらがなに変換"""
        return cls.re_katakana.sub(lambda x: chr(ord(x.group(0)) - 0x60), text)

    @classmethod
    def katakana(cls, text: str) -> str:
        """カタカナに変換"""
        return cls.re_hiragana.sub(lambda x: chr(ord(x.group(0)) + 0x60), text)

    @classmethod
    def hira_only(cls, text: str) -> str:
        """ひらがなに変換し，ひらがな以外を除去"""
        text = cls.hiragana(text)
        text = cls.re_non_hiragana.sub(u' ', text).strip()
        return text

    @classmethod
    def kakko(cls, text: str) -> str:
        """括弧を除去"""
        text = cls.re_kakko.sub(u'', text).strip()
        return text

    @classmethod
    def pre2(cls, text: str) -> str:
        """約物（句読点等）を除去"""
        text = cls.re_pre2.sub(u'', text).strip()
        text = text.translate(cls.tbl_zenhan_number)
        return text


class THOutOfRange(Exception):
    pass

#
#
# 駄洒落の種・変形音韻ペア検出のためのクラス
#
#


class THDetector(object):

    def __init__(self, written_path="written.csv") -> None:
        """
        引数:
            writen_path(str): 結果書き込みCSVのパス
        """
        self.result_pos = 0.0 # 採点用 (正例の再現率)
        self.result_neg = 0.0 # 採点用 (負例のFP比率)
        self.val = 0 # 閾値などの予備実験用可変値
        self.smoother = 0.00001
        self.written_path = written_path
        self.cnt = 0

    # <OBSOLETE>
    def printWithIndex(self, ary) -> str:
        return(u" ".join([u"%d:%s" % (i, it) for i, it in enumerate(ary)]))

    # 削除予定
    # 処理の流れの参考
    def loadMorphsLocs(self, line, prt=True) -> None:
        """モーラ音素列が他の部分と完全一致する形態素を抽出したものを画面出力"""
        #ret = []
        fpath_tmp = "tmp_mecab"
        _separator = u"S|!|S"
        self.morphsA = []
        self.phonsA = []
        self.locsA = []

        # 形態素解析
        #print(l.strip())
        with codecs.open(fpath_tmp, "w", encoding=enc) as ptr:
            ptr.write(line)
        cmd0 = "mecab -Owakati < %s | mecab -Oyomi" % (fpath_tmp, )
        cmd0_output = commands.getoutput(cmd0).decode(enc)
        cmd0_output_sep = cmd0_output.replace(u" ", _separator)
        # 記号とカッコ除去
        kakkozumi = Tr.pre2(Tr.kakko(cmd0_output_sep))
        #print("K:",kakkozumi)
        # 区切りの整理
        kakkozumi2 = kakkozumi.replace(_separator * 2, _separator)
        #print("K2:",kakkozumi2)
        # 長音処理
        chouonzumi = cp.chouon_new(kakkozumi2, _separator)
        # 【形態素毎の処理】:
        chouonzumiA = chouonzumi.split(_separator)
        for morph in chouonzumiA:
            if not morph:
                continue
            # ひらがな化->モーラ列化
            morph_moranized = cm.moratize(Tr.hira_only(morph))
            # 格納
            morphA = morph_moranized
            self.morphsA.append(morphA)
            self.phonsA += morphA
        loc = 0
        for mA in self.morphsA:
            lmA = len(mA)
            self.locsA.append((loc, loc + lmA))
            loc = loc + lmA
        if not prt:
            return
        print("M:", self.printWithIndex(self.morphsA))
        print("P:", self.printWithIndex(self.phonsA))
        print("L:", self.locsA)

    # <OBSOLETE>
    def findTane(self, matchFunc):
        """
        関数matchFuncを用いて，self.morphsAに格納されている形態素モーラ列と
        同じ文モーラ列内の他部分との一致を検索する．"""
        ret = []
        # 形態素について，全体モーラ列とのマッチ
        # モーラ列
        for i, mA in enumerate(self.morphsA):
            loc0, loc1 = self.locsA[i]
            agree = matchFunc(mA, self.phonsA, loc0, loc1)
            if not agree:
                continue
            ret.append(((loc0, loc1), agree))
        return ret

    # <OBSOLETE>
    def mainmainmain(self, fpath0, test_type="p"):
        """旧メインメソッドの一部
        正例または負例のいずれか一方が含まれたテキストファイルを読み込む．
        種・変形ペア候補を抽出する．

        """
        #stored_data1 = {}
        #stored_data2 = {}
        # 対象ファイルへの処理
        i = 0
        j = 0

        fname_stc_tmp = fpath0 + ".tmp" # 駄洒落文を保存する
        self.stc_tmp = {}
        print("processing [%s:%f] %s" % (test_type, self.val, fpath0,), end=" ")
        for l_ in codecs.open(fpath0, encoding=enc):
            i += 1
            self.cnt = i
            if i % 100 == 0:
                sys.stdout.write(".")
                sys.stdout.flush()
            line = l_.strip()
            # 駄洒落文の保存
            self.stc_tmp[i] = {"stc": line}
            #print(l.encode(enc))
            #n_ma = ""
            #n_ps = ""
            ## 種-変形候補の抽出
            self.loadMorphsLocs(line, False)
            self.stc_tmp[self.cnt]["loc"] = self.locsA
            self.stc_tmp[self.cnt]["phn"] = self.phonsA
            self.stc_tmp[self.cnt]["mph"] = self.morphsA
            #self.loadMorphsLocs(l, True)
            # 【モーラ列の完全一致 : モーラ無変化】
            #matchresult = self.findTane(ps.agree_exact)
            # 【モーラ列の一般化   : 子音の変更】
            #matchresult = self.findTane(ps.change_consonant)
            ## 【モーラ列の一般化   : 子音の変更 with 制限】
            matchresult = self.findTane(ps.ccWL_val(self.val))
            # 【音挿入＋モ列一般化 : 子音の変更 with 制限】
            #matchresult = self.findTane(ps.insertion_ccWL_val(self.val))
            if matchresult:
                for mr_pair in matchresult:
                    tane, henkeis = mr_pair
                    t_st, t_ed = tane
                    for henk in henkeis:
                        h_st, h_ed = henk
                j += 1
        pickle.dump(self.stc_tmp, open(fname_stc_tmp, "w")) # 駄洒落文保存
        if test_type == "p":
            score = j / 1.0 / i
            self.result_pos = score
            self.n_tp = j
            self.n_fn = i - j
        else:
            score = j / 1.0 / i
            self.result_neg = score
            self.n_fp = j
            self.n_tn = i - j

    # <OBSOLETE>
    def mainmain(self):
        """旧メインメソッドの一部．
        システム引数から正例ファイルと負例ファイルのパスを受ける．
        正例と負例のデータに対して分類性能を求める処理を実行する．"""
        if len(sys.argv) < 2:
            print("File must be specified!")
            return
        f_pos, f_neg = sys.argv[1], sys.argv[2]
        fpw = open(self.written_path, "w")
        for val in range(6, 153):
            self.val = 1.0 / (float(val) / 3.0)
            for test_type, fpath0 in [("p", f_pos), ("n", f_neg)]:
                self.mainmainmain(fpath0, test_type)
            precision = self.n_tp / 1.0 / (self.smoother + self.n_tp + self.n_fp)
            #print("prec:", precision)
            recall = self.n_tp / 1.0 / (self.n_tp + self.n_fn)
            #print("recl:", recall)
            accuracy = float(self.n_tp + self.n_tn) / (self.n_tp + self.n_tn + self.n_fp + self.n_fn)
            #print("accu:", accuracy)
            fvalue = 2.0 * precision * recall / (precision + recall)
            ltw = "%d,%f,%f,%f,%f" % (self.val, precision, recall, accuracy, fvalue)
            print(ltw)
            fpw.write(ltw + "\n")
            fpw.flush()
        fpw.close()
