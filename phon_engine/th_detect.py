#!/usr/bin/env python
#coding: utf-8
"""種・変形表現の探索に基づく駄洒落の検出スクリプト
結果保存先は最下部のmain()にて決定"""
from __future__ import print_function

import phon_engine.phon_similarity_2 as ps
import phon_engine.tables as tables
from phon_engine.th_detect_common import THDetector

enc = "utf-8"

reverse = {}
for table in (tables.phon_tbl0, tables.phon_tbl1, tables.phon_tbl2):
    for k, v in table.items():
        vow = v[-1:]
        cns = v[:-1]
        reverse[(cns, vow)] = k


def toHira(morae):
    ret = u""
    for cns, vow in morae:
        ret += reverse[(cns, vow)]
    return ret


class THDetectorNew(THDetector):

    def findTane2insert(self, cooked, djr_raw, mode="w", thres=0.0):
        """【形態素内1音挿入; epenthesis】形態素境界付音素列 cooked の中の
        音素の類似に基づいた種変形表現の検出結果を返す.

    入力:
            cooked -- "||"で区切られたモーラ音素列 (str)
            djr_raw -- 入力駄洒落文 (unicode)
            mode -- "w" なら1形態素と全体の一致, "m" は各形態素に対する一致
            thres -- 類似度合計の閾値
        帰り値:
            retA -- 一致部分のモーラ音素位置タプルのリスト

        依存関係はfindTane2()に加え:
            phon_similarity_2.MOR_TBL012

        [151015] 制約を追加：
            表層文字列の一致部分数 < 音韻列の一致部分数

        """

        retA = []
        showA = []
        mora_U = cooked
        moraAA = ps.ippankaWithRest2(mora_U)
        st = 0

        if mode == "w":
            moraSA = sum(moraAA, []) # 全形態素を繋げたモーラ列
            #print("SA:", moraSA)#d
            for mm in moraAA:
                #print("mm:", mm)#d
                # 中間部一音挿入 (っ，長音，ん Q [AEIOU] N)
                # TODO: ここを直す．長音の処理ルーチンを追加．
                # 促音（っ Q）と撥音（ん N）はただ追加する．
                # 長音は，次のように:
                # cooked で長音は母音化されてるので，母音1音の延長をする
                #for m_ins, priority in moras_all_pri.items():
                #    if priority < 5:
                #        continue
                for m_ins in (u'N', u'Q', u'-', None):
                    for i in range(1, len(mm)):
                        ed = st + len(mm) + 1  # st : mm の開始位置
                        if m_ins:
                            if m_ins == u"-": # 長音(=前モーラ母音)の挿入
                                if moraSA[i - 1][-1] in (u"Q", u"N", ):
                                    # 促・撥音は連続させない
                                    #print("stop 1")#d
                                    continue
                                m_ins_ = [(u'', moraSA[i - 1][-1])]
                                # 前モーラ母音
                            elif m_ins is not None:
                                m_ins_ = [(u"", m_ins)]
                                # ("", u"Q") の形
                        else:
                            m_ins_ = []
                        mmI = mm[:i] + m_ins_ + mm[i:] # 挿入
                        rsltA = ps.agree_threshold2(smallAT=mmI,
                                                    largeAT=moraSA,
                                                    st=st, ed=ed,
                                                    thres=thres,
                                                    use_vow_edist=False,
                                                    use_cns_obexr=True)
                        #numagreemora = len(rsltA)
                        retA += rsltA
                        if rsltA:
                            showA += [(mm, rsltA)]
                st += len(mm) + 1
        # 種表現・変形表現候補を表示
        if showA:
            rsltA_prev = []
            for mm, rsltA in showA:
                if rsltA == rsltA_prev:
                    continue
                print(djr_raw.replace(" || ", ""), end=": ")
                print(toHira(mm), end=",")
                print("-".join([toHira(moraSA[st:ed]) for (st, ed) in rsltA]))
                rsltA_prev = rsltA
        else:
            print()
        # 制約:
        #   一致形態素表層数 < 一致音韻区間数 <- 入れると精度激落ち (′・ω・`）
        #if not (numagreemorph <= numagreemora):
        #    return []
        # 1形態素と他の個々の形態素(同一形態素内の部分文字列)との一致
        # TODO
        return retA

    def findTane2I2(self, mora_U, mode="m", scr=0.0):

        retA = []
        moraAA = ps.ippankaWithRest2(mora_U) #
        st = 0
        thres = scr # the value must be within [0, 1]

        # 1形態素と全体との一致
        if mode == "w":
            moraSA = sum(moraAA, [])
            #print(moraSA)#d
            for mm in moraAA:
                ed = st + len(mm)  # st : mm の開始位置
                retA += ps.agree_threshold2(mm, moraSA, st, ed, thres,
                                            True, True, None)
                st += len(mm)

        # 1形態素と他形態素(or 同一形態素内の部分文字列同士)の一致
        elif mode == "m":
            for i, mm0 in enumerate(moraAA):
                st = 0
                for j, mm1 in enumerate(moraAA):
                    if i == j:
                        continue
                    ed = st + len(mm1)
                    retA += ps.agree_threshold2(mm1, mm0, st, ed, thres, True, True)
                    st += len(mm1)
        return retA

    def findTane_kana(self, mora_S, mode="m", scr=0.0):
        """解析例: ("手長エビとトリッパのタリオリーニ")

        Params:
            mora_S -- 形態素境界 || で区切られたモーラ列(str)
            mode -- "w" ならば1形態素と全体との一致を，
                    "m" ならば1形態素と他形態素の部分との一致(TODO)を行う
        Returns:
            retA -- 一致個所の範囲タプルを要素とするリスト"""

        retA = []
        #mora_U = mora_S.decode(enc)
        mora_U = mora_S
        # 形態素境界 -> 各mora列形態素
        # mora_gAA: 形態素毎の母音列
        # restAA  :     〃    子音列
        # 要素数 == 形態素境界数 + 1
        ## 一般化 (促音便を遺す)
        # mora_gSA: 全体の母音列
        # restSA  :   〃  子音列
        #moraAA = ps.ippankaWithRest2(mora_U) #
        moraAA = mora_U.split(u" || ")
        st = 0
        #thres = scr # the value must be within [0, 1]

        # 1形態素と全体(当該形態素以外)との一致
        if mode == "w":
            #moraSA = sum(moraAA, [])
            moraSA = u"".join(moraAA)
            for mm in moraAA:
                ed = st + len(mm)  # st : mm の開始位置
                retA += ps.agree_NOthreshold(mm, moraSA, st, ed)
                st += len(mm)

        # 1形態素と他形態素(or 同一形態素内の部分文字列同士)の一致
        # TODO
        return retA

    def findTane2withScore(self, mora_S, mode="w", scr=0.0):
        """下のfindTane2()に追加してスコアも返すようにしたもの"""


        retA = []
        scoreA = []
        mora_U = mora_S
        moraAA = ps.ippankaWithRest2(mora_U) #
        st = 0
        thres = scr # the value must be within [0, 1]

        # 1形態素と全体との一致
        if mode == "w":
            moraSA = sum(moraAA, [])
            for mm in moraAA:
                ed = st + len(mm)  # st : mm の開始位置
                tuples, score = ps.agree_threshold2_withScore(smallAT=mm,
                                                              largeAT=moraSA,
                                                              st=st,
                                                              ed=ed,
                                                              thres=thres,
                                                              use_vow_edist=False, # must be set to False
                                                              use_cns_obexr=True)
                retA += tuples
                scoreA += [score]
                st += len(mm)

        # 1形態素と他形態素(or 同一形態素内の部分文字列同士)の一致
        elif mode == "m":
            for i, mm0 in enumerate(moraAA):
                st = 0
                for j, mm1 in enumerate(moraAA):
                    if i == j:
                        continue
                    ed = st + len(mm1)
                    tuples, score = ps.agree_threshold2_withScore(
                        smallAT=mm1,
                        largeAT=mm0,
                        st=st,
                        ed=ed,
                        thres=thres,
                        use_vow_edist=False, # must be set to False
                        use_cns_obexr=True)
                    st += len(mm1)
                    retA += tuples
                    scoreA += [score]
        return retA, scoreA

    def findTane2(self, mora_S, mode="m", scr=0.0):
        """形態素境界付音素列 mora_S の中の
        音素の類似に基づいた種変形表現の検出結果を返す.

        modeはwとmとがあり，変形表現の範囲の左右限の取り方を示す．
        w: 形態素境界を無視した全モーラ．
        m: 形態素境界をまたがず同一形態素内に変形表現が含まれる．(DEFAULT)

        入力:
            mora_S: モーラ化済み駄洒落
            mode: 上記モード
            scr: しきい値
        返り値:
            retA:
        呼んでいる外部関数:
            phon_similarity_2.ippankaWithRest2()
            phon_siimilarity_2.agree_threshold()

        解析例: ("手長エビとトリッパのタリオリーニ")
ls_tane te na ga || e bi || to || to ri Q pa || no || ta ri o ri i ni ||
mora [[u'e', u'a', u'a'], [u'e', u'i'], [u'o'], [u'o', u'i', u'Q', u'a'], [u'o'],
[u'a', u'i', u'o', u'i', u'i', u'i'], [u'']]
rest [[u't', u'n', u'g'], [u'', u'b'], [u't'], [u't', u'r', u'', u'p'], [u'n'],
[u't', u'r', u'', u'r', u'', u'n'], [u'']]"""


        retA = []
        #mora_U = mora_S.decode(enc)
        mora_U = mora_S
        # 形態素境界 -> 各mora列形態素
        # mora_gAA: 形態素毎の母音列
        # restAA  :     〃    子音列
        # 要素数 == 形態素境界数 + 1
        ## 一般化 (促音便を遺す)
        # mora_gSA: 全体の母音列
        # restSA  :   〃  子音列
        moraAA = ps.ippankaWithRest2(mora_U) #
        #print(moraAA)#d
        st = 0
        thres = scr # the value must be within [0, 1]

        # 1形態素と全体との一致
        if mode == "w":
            moraSA = sum(moraAA, [])
            for mm in moraAA:
                ed = st + len(mm)  # st : mm の開始位置
                #retA += ps.agree_threshold2(mm, moraSA, st, ed, thres, None, funcname)
                retA += ps.agree_threshold2(mm, moraSA, st, ed, thres,
                                            False, True, None)
                st += len(mm)

        # 1形態素と他形態素(同一形態素内の部分文字列)との一致
        elif mode == "m":
            for i, mm0 in enumerate(moraAA):
                st = 0
                for j, mm1 in enumerate(moraAA):
                    if i == j:
                        continue
                    ed = st + len(mm1)
                    retA += ps.agree_threshold2(mm1, mm0, st, ed, thres, True, True)
                    st += len(mm1)
        #print(retA)
        return retA


#
#
# 以下（というか，本プログラム単体）は評価実験用には使用禁止
#
#

def main():
    #o = THDetector(written_path="141024_ccWL_thresVal_result_RELATIVE.txt")
    o = THDetectorNew(written_path="141024_ccWL_thresVal_result_RELATIVE.txt")
    try:
        o.mainmain()
    except (KeyboardInterrupt, EOFError):
        pass

if __name__ == "__main__":
    main()
