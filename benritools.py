# coding: utf-8
"""Convenient tools"""

from __future__ import print_function
import sys
#import codecs
import time
from datetime import timedelta

ENC = "utf-8"

class MyTimer(object):
    def __init__(self, tgt, itername="iteration", rpt=True):
        self.tgt = tgt
        self.rpt = rpt
        self.itn = itername
        self.itrs = 0
        self.sec = 0.0
    def __enter__(self):
        self.start = time.time()
        print(self.tgt, file=sys.stderr)
        return self
    def __exit__(self, *args):
        sec = time.time() - self.start
        self.sec = sec
        eff = "" if not self.itrs else\
               "/ {0} --> {1:.9f} ms / {2}".format(
                    self.itrs, float(sec) / self.itrs * 1000.0, self.itn)
        if self.rpt:
            print("{0} took {1:.2f} sec. {2}".format(self.tgt, sec, eff),
                    file=sys.stderr)

def timer_start(ret=False):
    print("#####", "STARTED:", time.strftime("%b %d %Y %H:%M:%S"))
    if ret:
        return time.time()

def timer_stop(time_prev=None):
    if not time_prev:
        time_prev = timer_start(True)
    print("#####", "FINISHED:", time.strftime("%b %d %Y %H:%M:%S"))
    if time_prev:
        td = timedelta(seconds=int(time.time() - time_prev))
        print("##### ELAPSED TIME:", str(td))

def printe(*args, **kwargs):
    kwargs['flush'] = True
    kwargs['file'] = sys.stderr
    print(*args, **kwargs)

def report_progress(i, num=100, show_time=False, time_prev=None, only_time=False):
    """Returns: new current time obj of class time.time()"""
    tm = ""
    if (i + 1) % num == 0:
        if show_time:
            if time_prev is not None:
                assert type(time_prev) == list and len(time_prev) == 2
                td_prev = timedelta(seconds=int(time.time() - time_prev[0]))
                td_start = timedelta(seconds=int(time.time() - time_prev[1]))
                time_prev[0] = time.time()
                tm = "** Elapsed time: (since last iter) {} (since start) {}".format(
                        str(td_prev), str(td_start))
            else:
                tm = "** time now: {}".format(
                        time.strftime("%b %d %Y %H:%M:%S"))

        if not only_time:
            printe("{0:6d} items done {1}".format(i + 1, tm))
        else:
            printe(tm)


    return time.time()

class ShColor:
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    PURPLE = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    BOLD = '\038[1m'
    UNDERLINE = '\033[4m'
    INVISIBLE = '\033[08m'
    REVERSE = '\033[07m'
    END = '\033[0m'
