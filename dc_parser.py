#!/usr/bin/env python
# coding: utf-8
"""Class for loading, editing or just parsing corpora/data
using the format of Dajare Corpus (DC) by Araki et al (2018).
Initially designed for DC version 8.

The format of the Corpus is like as follows:

```dajare_corpus
# Comment line
2162 (店頭)12 の [テント]1 、 [転倒]2 21
2163 セルジオ 、 ち 4
2171 (白菜) の ぬ か 漬 、 [はぁ 、 臭い] 2
2192 (血行) の よい [コケッコー] 1
```

1: Pairwise pun (perferct)  --> used for creating WPSM
2: Pairwise pun (imperfect) --> used for creating WPSM
3: Non-pairwise pun (Chojogata)
4: No puns contained

"""

import codecs
import re
import sys
import csv

#from phon_engine.chouon_process import chouon_obj
#from phon_engine.convert_mora import moratize
from phon_engine.oyomi import cook
from phon_engine.mora import Mora

# Type hinting
from typing import Generator, Iterable, List, Tuple, Union, Any

from icecream import ic  # Debug

# Define type hinting
TaneHenkeiTuple = Tuple[List[Mora], List[Mora]]

# 文字コード用
ENC_DC = "utf-8"


def DC_get_mora_list(text: str, text_wakati: str) -> List[Mora]:
    """DCから得たテキストをMora列に変換．(形態素区切りを無視)"""
    text_nospc = ic(RE_DC.Space.sub("", text))
    match_add_info = RE_DC.ModYomi.search(text)
    ret: List[Mora] = []

    mod_yomi = "", None
    if match_add_info:
        #add_kakkotsuki = re.sub(r" +", "", match_add_info.group(0))
        # text == before_yomi + mod_yomi + after_yomi
        ba_yomi_split = [t.strip() for t in RE_DC.ModYomi.split(text, maxsplit=1)]
        ic(ba_yomi_split)
        before_yomi, after_yomi = ba_yomi_split[0], ba_yomi_split[-1]
        mod_yomi = match_add_info.group(1).strip()
        ic(before_yomi, mod_yomi)
        # 追加済み表記が複数形態素ならその個数を取る
        # TODO: !!作業中!! 1をkにする．kは読み注釈内の形態素数
        # (mod_yomi_loc - k) 番目の形態素の読みは mod_yomi に置換
        #before_yomiとmod_yomiとafter_yomiのそれぞれに調理を実施してmod_yomiの形態素数分をビフォアの末尾に上書きし，それにafterをつなげる
        before_yomi_cooked = cook(before_yomi, get_morphemes=True, as_list=True)
        mod_yomi_cooked = cook(mod_yomi, get_morphemes=True, as_list=True, yomi_is_given=True)
        ic(before_yomi_cooked, mod_yomi_cooked)
        after_yomi_cooked = cook(after_yomi, get_morphemes=True, as_list=True)
        ic(after_yomi, after_yomi_cooked)
        # TODO: 読み情報付与において対象の形態素数よりも多い形態素数が存在する不具合の抜本的対処
        # e.g.: '姻戚' という 1 形態素に対して <いんせ き> と 2 形態素を割当ててしまっている
        #     --> DC 作製時に両者の一致を取れていないことが原因と考えられる．
        #     --> 急場凌ぎとして，対象形態素数に読み付与形態素数を強制的に一致させる．
        #try:
        #    if len(before_yomi_cooked) < len(mod_yomi_cooked):
        #        raise DCGeneralFailure(("Num of Mod Yomi morphemes must be less than or equal to preceding morphemes!"
        #                                f" -- '{before_yomi}' VS '{mod_yomi}'"))
        #    before_yomi_cooked = before_yomi_cooked[:-len(mod_yomi_cooked)] # + mod_yomi_cooked
        #except DCGeneralFailure:  # 例外処理をしたい
        #    pass
        while len(before_yomi_cooked) < len(mod_yomi_cooked):
            # mod_yomiの最初の形態素と2番目の形態素を結合する
            mod_yomi_cooked_new = [mod_yomi_cooked[0] + mod_yomi_cooked[1]] + [mod_yomi_cooked[2:]]\
                if len(mod_yomi_cooked) > 2 else []
            mod_yomi_cooked = mod_yomi_cooked_new
        ic(before_yomi_cooked)
        for morph_moras in before_yomi_cooked + mod_yomi_cooked + after_yomi_cooked:
            ret.extend(morph_moras)
        return ret
    ret = cook(RE_DC.AddInfo.sub("", text_nospc), get_morphemes=False)
    ic(ret)
    ic(text_nospc)
    return ret


class DCGeneralFailure(Exception):
    """DCのアノテーションが原因と考えられる問題〔未分類〕"""


class RE_DC(object):
    """DCのパース用正規表現"""
    # コメント行
    Comment  = re.compile(r"^#")
    # 第0列 駄洒落ID
    DajareID = re.compile(r"^\d+")
    # 第3列 文種
    StcType  = re.compile(r".*(\d+)$")
    # 種表現アノテーション
    Tane     = re.compile(r"\((.*?)\)(\d*)")
    # 変形表現アノテーション
    Henkei   = re.compile(r"\[(.*?)\](\d*)")
    # 読み変更 --> (当面)無視
    ModYomi  = re.compile(r"<([\s\u3041-\u309F\u30a1-\u30f3]+)>")
    # 追記情報 --> (当面)無視
    AddInfo  = re.compile(r"<(.*?)>")
    # アノテーションに使用される文字のクリーンアップ用1
    FunctSep = re.compile(r" \[| \(|\]\d* |\)\d* ")  # スペース1個へ置換
    # アノテーションに使用される文字のクリーンアップ用2
    FunctChr = re.compile(r"[<>]")
    # 空白文字検出用
    Space    = re.compile(r"\s+")


class DCDajare(object):
    """駄洒落1文のデータクラス"""

    def __init__(self,
                 text_raw: str = "",
                 text_annotd: str = "",
                 th_pairs: List[TaneHenkeiTuple] = [],
                 context: List[Any] = [],
                 dajare_id: int = 0
                 ) -> None:
        self.text_raw = text_raw
        self.text_annotd = text_annotd
        self.th_pairs = th_pairs
        self.context = context if context else len(th_pairs) * [None]
        self.dajare_id = dajare_id
        assert len(th_pairs) == len(self.context), ("Number of contexts differs from that of TH pairs"
                                                    f"({len(th_pairs)} vs {len(self.context)})")

    def set_dajare_id(self, djrid: Union[str, int, float]) -> None:
        self.dajare_id = int(djrid)


class DCDataStore(object):
    """DajareCorpus用データストア"""

    def __init__(self) -> None:
        self.dajare_all: List[DCDajare] = []
        self.th_pairs_all: List[TaneHenkeiTuple] = []

    def add_dajare(self, d: Union[DCDajare, List[DCDajare]]) -> None:
        """駄洒落 (DCDajare) を追加"""
        if type(d) is DCDajare:
            self.dajare_all.append(d)
        else:
            self.dajare_all.extend(d)

    def add_th_pairs(self, thpair: Union[TaneHenkeiTuple, Iterable[TaneHenkeiTuple]]) -> None:
        """種変形ペア (Mora列のタプル，またはそのリスト) を受け取り追加"""
        if type(thpair) is not tuple:
            self.th_pairs_all.extend((t, h) for t, h in thpair if t and h)
        elif thpair[0] and thpair[1]:
            self.th_pairs_all.append(thpair)


class DCParser(object):
    """駄洒落コーパスパーサー"""

    def __init__(self, fpath: str, data_store: DCDataStore) -> None:
        self.ds = data_store # TH pairs
        self.set_corpus_path(fpath)

    def set_corpus_path(self, fpath_corpus: str) -> None:
        self.fpath = fpath_corpus
        print("N: Opened DJR corpus file: " + fpath_corpus, file=sys.stderr)

    def set_parsed_count(self, count: int = 0):
        self.parsed_count = count

    def parse_raw_vocab(self) -> Generator[Tuple[str, List[str]], None, None]:
        """(未修正 **TODO** ) コーパステキストより種類ラベルと形態素文字列のリストを抽出"""
        for i, line_ in enumerate(codecs.open(self.fpath, encoding=ENC_DC)):
            if (i + 1) % 10000 == 0:
                print("Read {:6d} lines".format(i + 1))
            if RE_DC.Comment.match(line_):
                continue
            line = line_.strip()
            stctype_match = RE_DC.StcType.match(line)
            line_nokakko = RE_DC.FunctSep.sub(u" ", RE_DC.FunctChr.sub("", line))
            tokens = line_nokakko.split()[1:-1] if stctype_match else line_nokakko.split()[1:]
            label = line_nokakko.split()[-1]
            yield label, tokens

    def parse_single(self,
                     stctypes: List[int],
                     genbun: str,
                     wakati: str) -> DCDajare:
        """コーパスデータ1行をパースしてDCDajareオブジェクトを返す"""
        th_extracted: List[TaneHenkeiTuple] = []
        tane_found = RE_DC.Tane.findall(wakati)
        henkei_found = RE_DC.Henkei.findall(wakati)

        # 種表現が単一の場合
        if len(tane_found) == 1:
            tane_raw = tane_found[0][0]
            #ic(tane_raw)
            tane_moras = DC_get_mora_list(tane_raw, wakati)
            if not tane_moras:
                return None # Quit processing this pun and go on
            for h_raw, h_i_str in henkei_found:
                if h_raw != u"":
                    henkei_moras = DC_get_mora_list(h_raw, wakati)
                    th_extracted.append((tane_moras, henkei_moras))
                    ic((tane_moras, henkei_moras))
        # 種表現が複数の場合
        elif len(tane_found) > 1:
            for t_raw, t_i_str in tane_found:
                # 添え字を考慮
                if not t_i_str:
                    continue
                t_i = int(t_i_str)
                tane_moras = DC_get_mora_list(t_raw, wakati)
                if not tane_moras:
                    continue
                for h_raw, h_i_str in henkei_found:
                    h_i = int(h_i_str)
                    if h_raw != u"" and t_i == h_i:
                        henkei_moras = DC_get_mora_list(h_raw, wakati)
                        if tane_moras and henkei_moras:
                            th_extracted.append((tane_moras, henkei_moras))
        return DCDajare(text_raw=genbun, text_annotd=wakati, th_pairs=th_extracted,
                        context=(len(th_extracted) * [None]))

    def parse_th_mora(self, verbose=False) -> None:
        """駄洒落コーパステキスト全体より駄洒落(DCDajare)を抽出してデータストアに保存"""
        cnt_processed_heichi = 0
        th_added = []

        with codecs.open(self.fpath) as f_dc:
            csv_rdr = csv.reader(f_dc, delimiter=',')
            for i, row in enumerate(csv_rdr):
                if verbose and (i + 1) % 1000 == 0:
                    print("Read {:6d} lines".format(i + 1))
                # コメント行
                if RE_DC.Comment.match(row[0]):
                    continue
                ic(i)

                # Type labels at the end of line
                stctypes = []
                stctypes_str = row[3]
                if stctypes_str != "":
                    stctypes = [int(char) for char in stctypes_str]
                    ic(stctypes)
                else:
                    ic(f"No data: line {i+1}")#d
                    continue
                # 駄洒落タイプによる制御 (併置型のみ)
                if not any(t in stctypes for t in {1, 2}):
                    continue
                djrid: str = row[0]
                genbun: str = row[1]
                wakati: str = row[2]
                ic(genbun)
                ic(wakati)
                parsed: DCDajare = self.parse_single(stctypes, genbun, wakati)
                parsed.set_dajare_id(djrid)
                assert type(parsed) is DCDajare, f"No DCDajare object: {i+1}th line"
                self.ds.add_dajare(parsed)
                th_added += parsed.th_pairs
                cnt_processed_heichi += 1
        cnt_processed_lines = i + 1
        cnt_added_thpair = len(th_added)
        # 種変形ペアリストに格納(追加)
        self.ds.add_th_pairs(th_added)
        ic(cnt_processed_lines)
        ic(cnt_processed_heichi)
        ic(cnt_added_thpair)
        self.set_parsed_count(cnt_processed_heichi)


def parsetest(fpath_corpus: str, dcs: DCDataStore) -> None:
    ic(fpath_corpus)
    dcp = DCParser(fpath_corpus, dcs)
    dcp.parse_th_mora()
    print(f"Parsed '{fpath_corpus}' ({dcp.parsed_count} items) successfully.")
    try:
        for i, d in enumerate(dcs.dajare_all):
            print(f" DC_ID[{i+1}]: {d.dajare_id}")
            print(f"Genbun[{i+1}]: {d.text_raw}")
            print(f"Annotd[{i+1}]: {d.text_annotd}")
            print(f"#ofTHP[{i+1}]: {len(d.th_pairs)}")
            for j, (tane, henkei) in enumerate(d.th_pairs):
                index2 = f"[{j+1}]" if len(d.th_pairs) > 1 else ""
                print(f"     Tane[{i+1}]{index2}: {tane}")
                print(f"   Henkei[{i+1}]{index2}: {henkei}")
                print(f"  Context[{i+1}]{index2}: {d.context[j]}")
            input("")
    except (EOFError, KeyboardInterrupt):
        pass


def main() -> int:
    if len(argv) < 1:
        return 1
    parsetest(argv[0], dcs_default)
    return 0


if __name__ == "__main__":
    argv = sys.argv[1:]
    dcs_default = DCDataStore()
    sys.exit(main())
